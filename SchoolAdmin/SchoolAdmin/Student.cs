﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;

namespace SchoolAdmin
{
    public class Student : Persoon
    {

        //attributen

        
        private Dictionary<DateTime, string> dossier = new Dictionary<DateTime, string>();


        //Properties


        public ImmutableList<VakInschrijving> VakInschrijvingen
        {
            get
            {
                List<VakInschrijving> temp = new List<VakInschrijving>();

                foreach (var el in VakInschrijving.AlleVakInschrijvingen)
                {

                    if (this.Equals(el.Student))
                    {
                        temp.Add(el);
                    }
                }

                return temp.ToImmutableList<VakInschrijving>();
            }
        }

        public ImmutableList<Cursus> Cursussen
        {
            get
            {

                List<Cursus> temp = new List<Cursus>();
                foreach (var el in VakInschrijving.AlleVakInschrijvingen)
                {
                    if (this.Equals(el.Student))
                    {
                        temp.Add(el.Cursus);
                    }
                }
                return temp.ToImmutableList<Cursus>();
            }
        }

        public override string GenereerNaamkaartje()
        {
           return $"{this.Naam} (STUDENT)";
              
        }

        public override double BepaalWerkbelasting()
        {
            double totaal = 0.0;

            foreach (var item in VakInschrijvingen)
            {
                if (item != null)
                {
                    totaal = totaal + 10;
                }
            }

            return totaal;
        }

        public ImmutableDictionary<DateTime,string> Dossier
        {
            get
            {
                return dossier.ToImmutableDictionary();
            }
        }
      

        // Constructor


        public Student(string naam, DateTime geboortedatum) :base(naam,geboortedatum)
        {
          
        }


        public void RegistreerCursusResultaat(Cursus cursus, byte? punten)
        {
            new VakInschrijving(cursus, punten, this);

        }


        public void Kwoteer(Cursus cursus, byte cijfer)
        {
            if (cijfer < 0 || cijfer > 20)  // aanvullen met extra voorwaarden
            {
                Console.WriteLine("Ongeldig cijfer!");

            }
            else
            {
                foreach (var el in VakInschrijving.AlleVakInschrijvingen)
                {
                    if (this  == el.Student && cursus == el.Cursus)
                    {
                        el.Resultaat = cijfer;
                    }
                }
                
            }
        }


        public double Gemiddelde()
        {
            double result = 0;
            int aantal = 0;

            foreach (var item in VakInschrijvingen)
            {
                if (item.Resultaat != null)
                {
                    result = result + (byte)item.Resultaat;          // kan niet mneer null zijn dus je mag (moet) casten naar een type dat je kan optellen
                    aantal++;

                }
            }

            return result / aantal;
        }


        public void ToonOverzicht()
        {
            
            Console.WriteLine($"{this.Naam}, {this.Leeftijd} jaar\n\nCijferrapport:\n***********");
          

            foreach (var item in VakInschrijvingen)
            {
              
               Console.WriteLine($"{item.Cursus.Titel}\t {item.Resultaat}");
                
            }
            Console.WriteLine($"Gemiddelde:\t {this.Gemiddelde():F1}\n");
        }


        public static Student StudentUitTekstFormaat(string csvWaarde)
        {
            string[] gesplitst = csvWaarde.Split(";");

            Student aangemaaktviacsv = new Student(gesplitst[0], new DateTime(Convert.ToInt32(gesplitst[3]), Convert.ToInt32(gesplitst[2]), Convert.ToInt32(gesplitst[1])));
            int i = 4;

            while (i < gesplitst.Length)
            {
                aangemaaktviacsv.RegistreerCursusResultaat(new Cursus(gesplitst[i]), Convert.ToByte(gesplitst[i + 1]));   //registreercursusresultaat is instantiemethode dus student.
                i = i + 2;
            }

            return aangemaaktviacsv;
        }


        public override string ToString()
        {
            return $"{base.ToString()}\nMeerbepaald, student";
        }


        public static void DemonstreerStudenten()
        {

            Student student1 = new Student("Said Aziz", new DateTime(2001, 1, 3));
            Student student2 = new Student("Mieke Vermeulen", new DateTime(2000, 1, 3));

            Cursus communicatie = new Cursus("Communicatie");
            Cursus programmeren = new Cursus("Programmeren");
            Cursus webtechnologie = new Cursus("Webtechnologie");
            Cursus databanken = new Cursus("Databanken");

            student1.RegistreerCursusResultaat(communicatie, 12);
            student1.RegistreerCursusResultaat(programmeren, null);
            student1.RegistreerCursusResultaat(webtechnologie, 13);
            student1.ToonOverzicht();

            student2.RegistreerCursusResultaat(communicatie, 13);
            student2.RegistreerCursusResultaat(programmeren, null);
            student2.RegistreerCursusResultaat(databanken, 14);
            student2.ToonOverzicht();

        }


        public static void DemonstreerStudentenUitTekstFormaat()

        {
            Student test = StudentUitTekstFormaat("Sam Camps;19;2;1981;Webtechnologie;14;Databanken;17;Netwerken;20");
            test.ToonOverzicht();
                                             //bestaat aangemaaktviacsv op dit moment nog? nee, want method dicht?

        }



        public static void StudentToevoegen()
        {
            Console.WriteLine("Naam van de student?");
            string naam = Console.ReadLine();
            Console.WriteLine("Geboortedatum van de student?");
            DateTime geboortedatum = DateTime.Parse(Console.ReadLine());

            new Student(naam, geboortedatum);
            
        }


        public static void ToonStudenten()
        {
            Console.WriteLine("1. Stijgend alfabetisch\n2. Dalend alfabetisch");
            string answer = Console.ReadLine();

            if (answer == "1")
            {
                ImmutableList<Student> gesorteerd = AlleStudenten.Sort(new StudentenNaamComparerOP());
                foreach (var item in gesorteerd)
                {
                    Console.WriteLine(item);
                }
            }

            else if (answer == "2")
            {
                 
                ImmutableList<Student> gesorteerd = AlleStudenten.Sort(new StudentenNaamComparerAF());
                foreach (var item in gesorteerd)
                {
                    Console.WriteLine(item);
                }
            }

        }

        public override string ToCSV()
        {
            string dossierentries = "";

            foreach (var item in this.Dossier)
            {
                dossierentries = dossier + $";{item.Key.ToString(new CultureInfo("nl-BE"))};\"{item.Value}\"";
            }


            return base.ToCSV() + dossierentries;

        }


    }


}