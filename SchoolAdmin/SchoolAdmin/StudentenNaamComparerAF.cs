﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace SchoolAdmin
{
    public class StudentenNaamComparerAF: IComparer<Student>
    {

        public StudentenNaamComparerAF()
        {
        }


        public int Compare([AllowNull] Student x, [AllowNull] Student y)
        {

            if (x.Naam.CompareTo(y.Naam) == 0)
            {
                return 0;

            }
            else if (x.Naam.CompareTo(y.Naam) == -1)
            {
                return 1;
            }

            else
            {
                return -1;
            }
       
        }

    }
}
