﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;


namespace SchoolAdmin
{
    public class Lector:Personeel
    {
        private Dictionary<Cursus, double> cursussen = new Dictionary<Cursus, double>();
        
        
        public override string GenereerNaamkaartje()
        {
            string temp = $"{this.Naam}\nLector voor:\n";

            foreach(var element in cursussen)
            {
                temp = temp + element.Key.Titel + $"\n";
          
            }
            return temp;     
        
        }


        public override double BepaalWerkbelasting()
        {
            double som = 0;
            foreach (var element in this.cursussen)
            {
                som = som + element.Value;
            }
            return som;
        }


        public override uint BerekenSalaris ()
        {
            return (uint)((2200 + ((this.Ancienniteit / 4) * 120)) * (BepaalWerkbelasting() / 40));
        }



        public Lector(string naam, DateTime geboortedatum, Dictionary<string, byte> taken) : base(naam, geboortedatum, taken)
        {

        }     


        public static void DemonstreerLector()
        {
            Lector anna = new Lector("Anna Bolzano", new DateTime(1975, 6, 12), new Dictionary<string, byte>());
            Cursus economie = new Cursus("Economie");
            Cursus statistiek = new Cursus("Statistiek");
            Cursus analytischeMeetkunde = new Cursus("Analytische Meetkunde");

            anna.cursussen.Add(economie, 3);
            anna.cursussen.Add(statistiek, 3);
            anna.cursussen.Add(analytischeMeetkunde, 4);
            anna.Ancienniteit = 9;

            foreach (var element in AlleAdministratiefPersoneel)
            {
                Console.WriteLine(element.GenereerNaamkaartje());
            }

            foreach (var element in AlleLectoren)
            {
                Console.WriteLine(element.GenereerNaamkaartje());
            }

            Console.WriteLine($"Werkbelasting is: {anna.BepaalWerkbelasting()}");
            Console.WriteLine($"Salaris is: {anna.BerekenSalaris()}");

        }

        public override string ToString()
        {
            return $"{base.ToString()}\nMeerbepaald, lector";

        }


        public override string ToCSV()
        {
            string cursussen = "";

            foreach (var item in this.cursussen)
            {
                cursussen = cursussen + $";{item.Key.Id};{item.Value}";
            }


            return base.ToCSV() + cursussen;

        }




    }
}
