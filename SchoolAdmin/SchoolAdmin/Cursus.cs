﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
namespace SchoolAdmin
{
    public class Cursus:ICSVSerializable
    {
        //attributen

        public string Titel;
        private int id;
        private static int maxId = 1;
        private byte studiepunten;

        private static List<Cursus> alleCursussen = new List<Cursus>();
      
        //constructors//////////////////////////////////////////////////////////


        public Cursus(string titel, byte studiepunten)
        {
            this.Titel = titel;
            this.id = maxId;
            this.studiepunten = studiepunten;

            foreach (var item in AlleCursussen)
            {
                if (item.Titel == titel)
                {
                    throw new DuplicateDataException(this, item,"Nieuwe cursus heeft dezelfde naam als een bestaande cursus"); 
                }
            }
        
            maxId++;
            registreerCursus(this);                                                     //klopt dit voor bij aanmaak object automatisch te registreren >ja
        }


        public Cursus(string titel) : this(titel, 3)
        {
        }


        //Properties////////////////////////////////////////////////////


        public ImmutableList<VakInschrijving> VakInschrijvingen
        {
            get
            {
                List<VakInschrijving> temp = new List<VakInschrijving>();
                foreach (var el in VakInschrijving.AlleVakInschrijvingen)
                    if (this.Equals(el.Cursus))
                    {
                        temp.Add(el);
                    }
                return temp.ToImmutableList();
            }
           
        }


        public ImmutableList<Student> Studenten
        {
            get
            {
                List<Student> temp = new List<Student>();
                foreach (var el in VakInschrijving.AlleVakInschrijvingen)
                {
                    if (this.Equals(el.Cursus))
                    {
                        temp.Add(el.Student);
                    }
                }
                    
                return temp.ToImmutableList();
            }
           
        }


        public static ImmutableList<Cursus> AlleCursussen
        {
            get
            {
                return alleCursussen.ToImmutableList();
            }
        }


        public int Id
        {
            get{
                return this.id;
            }
        }

        public byte Studiepunten
        {
            get
            {
                return this.studiepunten;
            }
            private set
            {
                this.studiepunten = value;
            }
        }


        //methods//////////////////////////////////////////////////


        public void ToonOverzicht()
        {
            Console.WriteLine($"{this.Titel} ({this.Id}) ({this.Studiepunten} stp)");

            foreach (var item in Studenten)
            {
                if (item != null)
                {
                    Console.WriteLine($"{item.Naam}");
                }
            }
            Console.WriteLine();
        }


        public static void DemonstreerCursussen()
        {
            Student student1 = new Student("Said Aziz", new DateTime(2001, 1, 3));
            Student student2 = new Student("Mieke Vermeulen", new DateTime(2000, 1, 3));

            Cursus communicatie = new Cursus("Communicatie");
            Cursus databanken = new Cursus("Databanken", 5);  
            Cursus programmeren = new Cursus("Programmeren");
            Cursus webtechnologie = new Cursus("Webtechnolgie", 6);

           // programmeren.Studenten = new List<Student>(new Student[] { student1, student2 }); // >> volgens de voorbeeldcorrectie maar dit staat nergens in de cursus
           // programmeren.Studenten = new List<Student> { student1, student2 };

            student1.RegistreerCursusResultaat(communicatie, 12);
            student1.RegistreerCursusResultaat(programmeren, null);
            student1.RegistreerCursusResultaat(webtechnologie, 13);

            student2.RegistreerCursusResultaat(communicatie, 13);
            student2.RegistreerCursusResultaat(programmeren, null);
            student2.RegistreerCursusResultaat(databanken, 14);

            communicatie.ToonOverzicht();
            databanken.ToonOverzicht();
            programmeren.ToonOverzicht();
            webtechnologie.ToonOverzicht();
           
        }

        public static void registreerCursus(Cursus cursus)
        {


            alleCursussen.Add(cursus);
        }


        public static Cursus ZoekCursusOpId(int id)
        {
                                                                           // wordt in voorbeeld oplossing niet op immutable gezet
                                                                           // maar moet wel want je returned een object
                                                                           //maar later voor toe te voegen moeten we kunnen verwijzen naar dne echte cursus
            foreach (var item in AlleCursussen)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }

            return null;
        }


        public override bool Equals(object obj)
        {

            if (obj is null)
            {
                return false;
            }


            if (obj is Cursus)
            {
                return this.id == ((Cursus)obj).id;
            }

            else
            {
                return false;
            }

        }

        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }

        public override string ToString()
        {
            return $"{this.Titel} ({this.Studiepunten})";
        }


        public static void CursusToevoegen()
        {
            Console.WriteLine("Titel van de cursus?");
            string titel = Console.ReadLine();
            Console.WriteLine("Aantal studiepunten?");
            byte stp = Convert.ToByte(Console.ReadLine());


            try
            {
                new Cursus(titel, stp);
            }

            catch (DuplicateDataException ex)
            {
                Console.WriteLine($"{ex.Message}\nID van de bestaande cursus is {((Cursus)ex.Waarde2).Id}");
            }
        }

        public static void ToonCursussen()
        {
            Console.WriteLine("1. Stijgend alfabetisch\n2. Volgens studiepunten");
            string answer = Console.ReadLine();


            if (answer == "1")
            {
                ImmutableList<Cursus> gesorteerd = AlleCursussen.Sort(new CursusNaamComparer());
                foreach (var item in gesorteerd)
                {
                    Console.WriteLine(item);
                }

            }

            else if (answer == "2")
            {
                ImmutableList<Cursus> gesorteerd = AlleCursussen.Sort(new CursusSTPComparer());
                
                foreach (var item in gesorteerd)
                {
                    Console.WriteLine(item);
                }
            }
        }

        public string ToCSV()
        {

            return $"Cursus;{this.Id}\"{this.Titel}\";{this.Studiepunten}";

        }


    }
}
