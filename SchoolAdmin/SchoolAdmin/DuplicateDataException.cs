﻿using System;
namespace SchoolAdmin
{
    public class DuplicateDataException:ApplicationException
    {


        object waarde1;
        object waarde2;

        public object Waarde1
        {
            get
            {
                return waarde1;
            }
            set
            {
                waarde1 = value;
            }
        }

        public object Waarde2
        {
            get
            {
                return waarde2;
            }
            set
            {
                waarde2 = value;
            }
        }


        public DuplicateDataException(object waarde1, object waarde2, string message) :base(message)
        {
            this.Waarde1 = waarde1;
            this.Waarde2 = waarde2;
            
        }
    }
}
