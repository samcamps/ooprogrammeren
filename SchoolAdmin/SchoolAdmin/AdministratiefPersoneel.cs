﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;


namespace SchoolAdmin
{
    public class AdministratiefPersoneel:Personeel
    {

       
        public override double BepaalWerkbelasting()
        {
           double som = 0;
           foreach (var element in this.taken)
            {
                som = som + element.Value;
            }
            return som;
        }


        public override uint BerekenSalaris()
        {
            return (uint)((2000 + ((this.Ancienniteit / 3)*75)) * (BepaalWerkbelasting() / 40));
        }

        public override string GenereerNaamkaartje()
        {
            return $"{this.Naam} (ADMINISTRATIE)";
        }


        public static void DemonstreerAdministratiefPersoneel()
        {
            AdministratiefPersoneel ahmed = new AdministratiefPersoneel("Ahmed Azzaoui", new DateTime(1988, 2,4),
                new Dictionary<string, byte> { { "roostering", 10 },{ "correspondentie", 10 } ,{ "animatie", 10 } });
            ahmed.Ancienniteit = 4;

            foreach(var element in AlleAdministratiefPersoneel)
            {
                Console.WriteLine(element.GenereerNaamkaartje());
            }

            Console.WriteLine($"Werkbelasting is: {ahmed.BepaalWerkbelasting()}");
            Console.WriteLine($"Salaris is: {ahmed.BerekenSalaris()}");
            //Console.WriteLine($"Ancieniteit: {ahmed.Ancienniteit}");
        }

        public override string ToString()
        {
            return $"{base.ToString()}\nMeerbepaald, administratief personeel";

        }

        //constructor

        public AdministratiefPersoneel(string naam, DateTime geboortedatum, Dictionary<string, byte> taken) : base(naam, geboortedatum,taken)
        {   
  
        }


        public override string ToCSV()
        {
            string taken = "";

            foreach(var item in this.Taken)
            {
                taken = taken + $";\"{item.Key}\";{item.Value}";
            }

        
               return base.ToCSV() + taken;

        }

    }
}
