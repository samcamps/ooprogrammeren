﻿using System;
namespace SchoolAdmin
{
    public class CapaciteitOverschredenException:ApplicationException
    {


        public CapaciteitOverschredenException(string message) : base(message)
        {

        }
    }
}
