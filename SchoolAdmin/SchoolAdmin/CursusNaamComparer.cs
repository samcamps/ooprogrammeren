﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace SchoolAdmin
{
    public class CursusNaamComparer : IComparer<Cursus>
    {
        public CursusNaamComparer()
        {
        }

        public int Compare([AllowNull] Cursus x, [AllowNull] Cursus y)
        {
            return x.Titel.CompareTo(y.Titel);
            
        }
    }
}
