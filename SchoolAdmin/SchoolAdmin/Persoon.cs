﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;


namespace SchoolAdmin
{
    public abstract class Persoon: ICSVSerializable
    {
        //attributen

        private uint id;
        private DateTime geboortedatum;
        private string naam;

        private static uint maxId = 1;
        private static List<Persoon> allePersonen = new List <Persoon>();


        //Properties
        public uint Id
        {
            get
            {
                return this.id;
            }
            private set
            {
                this.id = value;
            }
        }

        public DateTime Geboortedatum
        {
            get
            {
                return this.geboortedatum;
            }
        }


        public int Leeftijd
        {
            get
            {
                return DateTime.Now.Year - this.Geboortedatum.Year;
            }
        }

        public string Naam
        {
            get
            {
                return this.naam;
            }
            set
            {
                this.naam = value;
            }
        }

        public static ImmutableList<Persoon> AllePersonen
        {
            get
            {
                return allePersonen.ToImmutableList();
            }
        }


        public static ImmutableList<Persoon> AlleLectoren
        {
            get
            {
                List<Persoon> temp = new List<Persoon>();
                foreach(var el in allePersonen)
                {
                    if (el is Lector)
                    {
                        temp.Add(el);
                    }
                }
                return temp.ToImmutableList<Persoon>();
            }
        }
        
        public static ImmutableList<AdministratiefPersoneel> AlleAdministratiefPersoneel
        {
            get
            {
                List<AdministratiefPersoneel> temp = new List<AdministratiefPersoneel>();
                foreach (var el in allePersonen)
                {
                    if (el is AdministratiefPersoneel)
                    {
                        temp.Add((AdministratiefPersoneel)el);
                    }
                }
                return temp.ToImmutableList<AdministratiefPersoneel>();
            }
        }


        public static ImmutableList<Student> AlleStudenten
        {
            get
            {
                List<Student> temp = new List<Student>();
                foreach (var el in allePersonen)
                {
                    if (el is Student)
                    {
                        temp.Add((Student)el);
                    }
                }
                return temp.ToImmutableList<Student>();
            }
        }


        public abstract string GenereerNaamkaartje();
        public abstract double BepaalWerkbelasting();

        //Constructor
  
        public Persoon(string naam, DateTime geboortedatum)
        {
            this.Id = maxId;
            maxId++;
            this.Naam = naam;
            this.geboortedatum = geboortedatum;
            allePersonen.Add(this);

        }


        //override van equals op id

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }


            if (obj is Persoon)
            {
                if (this.id == ((Persoon)obj).id)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }

        }

        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }


        public override string ToString()
        {
            return $"Persoon\n--------\nNaam: {this.Naam}\nLeeftijd: {this.Leeftijd}";

        }

        public virtual string ToCSV()
        {
            return $"{this.GetType().Name};{this.Id};\"{this.Naam}\";{this.Geboortedatum.ToString(new CultureInfo("nl-BE"))}";
        }
    }
}
