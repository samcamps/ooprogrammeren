﻿using System;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            bool cont = true;

            while (cont)
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("0. Om te stoppen\n1. DemonstreerStudenten uitvoeren\n2. DemonstreerCursussen uitvoeren\n3. DemonstreerStudentUitTekstFormaat" +
                    "\n4. Demonstreer Studie programma 1\n5. Demonstreer Studie programma 2\n6. Demonstreer AdminPersoneel\n7. Demonstreer Lector" +
                    "\n8. Student Toevoegen\n9. Cursus Toevoegen \n10. Vakinschrijving toevoegen\n11. Inschrijvingsgegevens tonen\n12. Toon Studenten\n13. Toon Cursussen\n14. Exporteer Data");


                int keuze = Convert.ToInt32(Console.ReadLine());

                switch (keuze)
                {
                    case 0:
                        cont = false;
                        break;
                    case 1:
                        Student.DemonstreerStudenten();
                        break;

                    case 2:
                        Cursus.DemonstreerCursussen();
                        break;

                    case 3:
                        Student.DemonstreerStudentenUitTekstFormaat();
                        break;

                    case 4:
                        StudieProgramma.DemonstreerStudieProgramma1();
                        break;

                    case 5:
                        StudieProgramma.DemonstreerStudieProgramma2();
                        break;

                    case 6:
                        AdministratiefPersoneel.DemonstreerAdministratiefPersoneel();
                        break;

                    case 7:
                        Lector.DemonstreerLector();
                        break;

                    case 8:
                        Student.StudentToevoegen();
                        break;

                    case 9:
                        Cursus.CursusToevoegen();
                        break;

                    case 10:
                        VakInschrijving.VakInschrijvingToevoegen();
                        break;

                    case 11:

                        VakInschrijving.ToonInschrijvingen();
                        break;


                    case 12:
                        Student.ToonStudenten();
                        break;

                    case 13:
                        Cursus.ToonCursussen();
                        break;

                    case 14:
                        ExporteerData();
                        break;


                    case 15:
                        Test();
                        break;


                    case 100:
                        VakInschrijving.VakInschrijvingToevoegenNull();
                        break;

                    default:
                        Console.WriteLine("Is geen geldige keuze");
                        break;

                }
            }
        }

            public static void Test()
            {
            Persoon.AlleStudenten[0].Naam = null;
            foreach (var el in Persoon.AlleStudenten)
            {
                Console.WriteLine(el.Naam);

            }

        }

        public static void ExporteerData()
        {
            string csv = "";


            foreach (var item in Persoon.AllePersonen)
            {
                csv = csv + item.ToCSV() + "\n";

            }

            foreach (var item in Cursus.AlleCursussen)
            {
                csv = csv + item.ToCSV() + "\n";

            }


            Console.WriteLine(csv);

        }
            

        }
    }
