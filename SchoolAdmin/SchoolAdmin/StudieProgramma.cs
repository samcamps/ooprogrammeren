﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    public class StudieProgramma
    {

        private string naam;
        private Dictionary<Cursus,byte> cursussen;


        public StudieProgramma()
        {
           
        }

        public StudieProgramma(string naam)
        {
            this.naam = naam;
        }


        public string Naam
        {
            get
            {
                return this.naam;
            }
        
        }

        public Dictionary<Cursus,byte> Cursussen
        {
            get
            {
                return this.cursussen;
            }

            set
            {
                if (!(value is null))
                {
                    this.cursussen = value;
                }
                
            }
        }

        public void ToonOverzicht()
        {
            Console.WriteLine($"{this.Naam}: \n");
            foreach (var item in this.cursussen)
            {
                Console.WriteLine($"{item.Key.Titel} (sem.: {item.Value})");

            }
            Console.WriteLine();
        }


        public static void DemonstreerStudieProgramma1()
        {

            Cursus communicatie = new Cursus("Communicatie");
            Cursus programmeren = new Cursus("Programmeren");
            Cursus databanken = new Cursus("Databanken", 5);
            Dictionary<Cursus,byte> cursussen = new Dictionary<Cursus,byte> { { communicatie, 1 },{ programmeren, 1 },{ databanken, 1 } };
            Dictionary<Cursus, byte> cursussensnb = new Dictionary<Cursus, byte> { { communicatie, 2 }, { programmeren, 1 }, { databanken, 1 } };

            StudieProgramma programmerenProgramma = new StudieProgramma("Programmeren");
            StudieProgramma snbProgramma = new StudieProgramma("Systeem- en netwerkbeheer");
            programmerenProgramma.cursussen = cursussen;
            snbProgramma.cursussen = cursussensnb;
            // later wordt Databanken geschrapt uit het programma SNB
            //snbProgramma.cursussen[2] = null;                           // ook al checked de setter op null, je past hier de array rechtstreeks aan, wat kan maar geeft error geeft in toonoverzicht()
            programmerenProgramma.ToonOverzicht();
            snbProgramma.ToonOverzicht();

        }

        public static void DemonstreerStudieProgramma2()
        {
            Cursus communicatie = new Cursus("Communicatie");
            Cursus programmeren = new Cursus("Programmeren");
            Cursus databanken = new Cursus("Databanken", 5);
            Dictionary<Cursus,byte> cursussen1 = new Dictionary<Cursus,byte>(){ { communicatie, 1 }, { programmeren, 1 } ,{ databanken, 1 } };
            Dictionary<Cursus,byte> cursussen2 = new Dictionary<Cursus,byte>(){ { communicatie, 2 }, { programmeren, 1 },{ databanken,1 } };
            StudieProgramma programmerenProgramma = new StudieProgramma("Programmeren");
            StudieProgramma snbProgramma = new StudieProgramma("Systeem- en netwerkbeheer");
            programmerenProgramma.cursussen = cursussen1;
            snbProgramma.cursussen = cursussen2;
            // later wordt Databanken geschrapt uit het programma SNB
            // voor SNB wordt bovendien Programmeren hernoemd naar Scripting
            //snbProgramma.cursussen[2] = null;                                    //ja null mag hier niet
            snbProgramma.cursussen[programmeren] = 2;      //adlib value aangepast naaer 2e semester             // wordt in beide studieprogramma's aangepast want het object cursus wordt veranderd, wat hier eignelijk niet zou mogen kunnen 
            programmerenProgramma.ToonOverzicht();
            snbProgramma.ToonOverzicht();
        }

    }
}
