﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;


namespace SchoolAdmin
{
    public class VakInschrijving
    {
        private Student student;
        private Cursus cursus; 
        private byte? resultaat;

        private static List<VakInschrijving> alleVakInschrijvingen = new List<VakInschrijving>();
                        

        public static ImmutableList<VakInschrijving> AlleVakInschrijvingen
        {
            get
            {
                return alleVakInschrijvingen.ToImmutableList<VakInschrijving>();
            }
        }

        public Student Student
        {
            get
            {
                return student;
            }
            set
            {
                this.student = value;
            }
        }


        public byte? Resultaat
        {
            get
            {
                return this.resultaat;
            }
            set
            {
                if (value is null || (value >= 0 && value <= 20))
                {
                    this.resultaat = value;
                }
            }
        }

        public Cursus Cursus
        {
            get
            {
                return this.cursus;
            }
        }

        public VakInschrijving(Cursus cursus, byte? resultaat, Student student) {


            if (cursus is null || student is null)
            {
                throw new ArgumentException("Cursus of Student kan niet null zijn");
            }

            foreach(var item in AlleVakInschrijvingen)
            {
                if (item.Student == student && item.Cursus == cursus)
                {
                    throw new ArgumentException("Deze student is al voor dit vak ingeschreven");
                }
            }

           
            int i = 0;
            foreach (var item in AlleVakInschrijvingen)
            {
                if (item.Cursus == cursus && item.resultaat is null)
                {
                    i++;
                }
            }

            if (i == 20) { 
            
                throw new CapaciteitOverschredenException("Reeds 20 studenten voor deze cursus ingeschreven\nStudent wordt niet ingeschreven");
            }


            this.Resultaat = resultaat; 
            this.cursus = cursus;
            this.Student = student;
            alleVakInschrijvingen.Add(this);
       
        }



        public static void VakInschrijvingToevoegen()
        {
            //kies student

            Console.WriteLine("Welke Student? Enter het nummer");

            foreach (var el in Persoon.AlleStudenten)
            {

                Console.WriteLine($"{el.Id} {el}");
                Console.WriteLine();
            }

            int studentid = Convert.ToInt32(Console.ReadLine());
            
            //kies cursus

            Console.WriteLine("Welke cursus? Enter het nummer");

            foreach (var el in Cursus.AlleCursussen)
            {
                Console.WriteLine($"{el.Id} {el.Titel}");
            }

            int cursusid = Convert.ToInt32(Console.ReadLine());
            
            //al dan niet resultaat ingeven

            Console.WriteLine("Wil je een resultaat toekennen?");
            string antwoord = Console.ReadLine().ToLower();
            byte? resultaat;

            if (antwoord == "ja")
            {
                Console.WriteLine("Wat is het resulataat");
                resultaat = Convert.ToByte(Console.ReadLine());

            }
            else
            {
                resultaat = null; //niet perse nodig maar wel goed om expliciet te zeggen
            }


            try
            {
                new VakInschrijving(Cursus.ZoekCursusOpId(cursusid), resultaat, Persoon.AlleStudenten[studentid - 1]);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch(CapaciteitOverschredenException ex)
            {
                Console.WriteLine(ex.Message);
            }
 
        }


        public static void ToonInschrijvingen()
        {
            Console.WriteLine("Alle studenten in het systeem");

            foreach(var el in Persoon.AlleStudenten)
            {
                Console.WriteLine(el);
            }

            Console.WriteLine();
            Console.WriteLine("Alle cursussen in het systeem");
            foreach (var item in Cursus.AlleCursussen)
            {
                Console.WriteLine($"{item.Titel} met als studiepunten {item.Studiepunten}");
            }

            Console.WriteLine();
            Console.WriteLine("Alle Inschrijvingen in het systeem");

            foreach (var el in AlleVakInschrijvingen)
            {
                Console.WriteLine($"{el.Student}\nIngeschreven voor: {el.Cursus.Titel}");
            }


        }

        public static void VakInschrijvingToevoegenNull()
        {
            try
            {
                new VakInschrijving(null, null, null);
            }

            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }




    }
}
