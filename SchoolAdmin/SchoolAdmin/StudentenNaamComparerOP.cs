﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace SchoolAdmin
{
    public class StudentenNaamComparerOP: IComparer<Student>
    {

        public StudentenNaamComparerOP()
        {
        }




        public int Compare([AllowNull] Student x, [AllowNull] Student y)
        {

            return x.Naam.CompareTo(y.Naam);
        }
       
        

    }
}
