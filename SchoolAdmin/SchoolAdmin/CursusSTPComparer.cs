﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace SchoolAdmin
{
    public class CursusSTPComparer : IComparer<Cursus>
    {
        public CursusSTPComparer()
        {
        }

        public int Compare([AllowNull] Cursus x, [AllowNull] Cursus y)
        {
            if (x.Studiepunten < y.Studiepunten)
            {
                return -1;
            }

            else if (x.Studiepunten == y.Studiepunten)
            {
                return 0;
            }

            else
            {
                return 1;
            }
        }
    }
}
