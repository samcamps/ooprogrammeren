﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace SchoolAdmin
{
    public abstract class Personeel:Persoon
    {
        //attributen

       
        private byte ancienniteit;
        public Dictionary<string,byte> taken = new Dictionary<string,byte>();



        // properties

        public byte Ancienniteit
        {
            get
            {
                return this.ancienniteit;
            }
            set
            {
                if (value > 0 && value <= 50)
                {
                    this.ancienniteit = value;
                }
                else if (value >0)
                {
                    this.ancienniteit = 50;
                }
                else { Console.WriteLine("geen geldige waarde");
                }
                
            }
        }

        

        public ImmutableDictionary<string,byte> Taken
        {
            get
            {
                return taken.ToImmutableDictionary();
            }
        }


        public abstract uint BerekenSalaris();
          

        //constructor
        
        public Personeel(string naam, DateTime geboortedatum, Dictionary<string,byte> taken):base(naam,geboortedatum)
        {
            foreach(var element in taken)
            {
                this.taken.Add(element.Key, element.Value);
            }
      
        }


        
    }
}
