﻿using System;
namespace IndividueleOefeningen
{
    public class VierkantH14:RechtHoekH14
    {

        public double Zijde
        {
            get
            {
                return base.Breedte;
            }
            set
            {
                base.Breedte = value;
                base.Lengte = value;
            }
        }



        public VierkantH14(double zijde)
        {
            this.Zijde = zijde;
        }
    }
}
