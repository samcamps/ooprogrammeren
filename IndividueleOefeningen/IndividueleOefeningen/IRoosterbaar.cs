﻿using System;
namespace IndividueleOefeningen
{
    public interface IRoosterbaar
    {
        TimeSpan Tijdsduur
        {
            get;
        }

        string Omschrijving
        {
            get;
        }


        void Initialiseer();
        DateTime RoosterOm(DateTime referentiepunt);
    }
}
