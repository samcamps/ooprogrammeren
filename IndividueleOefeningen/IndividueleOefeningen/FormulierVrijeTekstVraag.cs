﻿using System;
namespace IndividueleOefeningen
{
    class FormulierVrijeTekstVraag:FormulierVraag
    {



        public override void Toonvraag()
        {
            Console.WriteLine($"{this.Tekst}");
            Console.WriteLine("Sluit af met ENTER");

        }

        public override void LeesAntwoord()
        {
            this.Antwoord = Console.ReadLine();
        }


        public FormulierVrijeTekstVraag(string tekst) :base(tekst)
        {

        }
    }
}
