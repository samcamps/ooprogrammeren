﻿using System;
namespace IndividueleOefeningen
{
    public class VergelijkbareRuit : VergelijkbareParallellogram
    {

     private double diagonaal1;
     private double diagonaal2;

     public double Diagonaal1
        {
            get
            {
                return this.diagonaal1;
            }
            set
            {
                this.diagonaal1 = value;
                Basis = UpdateBasis();
                Hoogte = UpdateHoogte();
          
            }
        }


        public double Diagonaal2
        {
            get
            {
                return this.diagonaal2;
            }
            set
            {
                this.diagonaal2 = value;
                Basis = UpdateBasis();
                Hoogte = UpdateHoogte();
            }
        }


        private double UpdateBasis()
        {

            return Math.Sqrt(Math.Pow(diagonaal1 / 2, 2) + Math.Pow(diagonaal2 / 2, 2));

        }

        private double UpdateHoogte()
        {

            return (diagonaal1 * diagonaal2) / (2 * (Math.Sqrt(Math.Pow(diagonaal1 / 2, 2) + Math.Pow(diagonaal2 / 2, 2))));

        }



        //public override double Oppervlakte
        //{
        //    get
        //    {
        //        return (this.Diagonaal1 * this.Diagonaal2) / 2;
        //    }
        //}




        public VergelijkbareRuit(double diagonaal1, double diagonaal2) : base(Math.Sqrt(Math.Pow(diagonaal1 / 2, 2) + Math.Pow(diagonaal2 / 2, 2)),( (diagonaal1 * diagonaal2) / (2 * (Math.Sqrt(Math.Pow(diagonaal1 / 2, 2) + Math.Pow(diagonaal2 / 2, 2))))))
        {
            this.Diagonaal1 = diagonaal1;
            this.Diagonaal2 = diagonaal2;
        }
        public override string ToString()
        {
            return $"Dit is een object van klasse {this.GetType().Name} met basis {this.Basis:F} en hoogte {this.Hoogte:F}";
        }


    }
}

