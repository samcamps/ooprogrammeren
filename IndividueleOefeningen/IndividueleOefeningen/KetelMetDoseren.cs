﻿using System;
namespace IndividueleOefeningen
{
    class KetelMetDoseren:Ketel, IVerwarmen, IWaterDoseren
    {

        public KetelMetDoseren(int inhoud):base(inhoud)
        {
        }


        public void Verwarmen()
        {
            Console.WriteLine("KetelMetDoseren - verwarmen");

        }

        public void WaterDoseren()
        {
            Console.WriteLine("KetelMetDoseren - water doseren");
        }
    }
}