﻿using System;
using System.Globalization;

namespace IndividueleOefeningen
{
    public class DateTimeOefeningen
    {
        public static void ToonSubmenu()
        {
            Console.WriteLine("Uit te voeren oefening?\n\n1. Dag van de week\n2. Ticks sinds 2000\n3. Schrikkelteller\n4. Simpele Timing" +
                "\n5. Verjaardag v2");


            int keuze = Convert.ToInt32(Console.ReadLine());


            switch (keuze)
            {

                case 1:
                    WeekdagProgramma();
                    break;

                case 2:
                    Ticks2000Programma();
                    break;

                case 3:
                    SchrikkeljaarProgramma();
                    break;

                case 4:
                    ArrayTimerProgramma();
                    break;

                case 5:
                    VerjaardagProgramma();
                    break;

          
                default:
                    Console.WriteLine("Dit is geen geldige keuze");
                    break;

            }
        }

        public static void WeekdagProgramma()
        {
            Console.WriteLine("Welke dag?");
            int dag = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Welke maand?");
            int maand = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Welk jaar?");
            int jaar = Convert.ToInt32(Console.ReadLine());

            DateTime mydate = new DateTime(jaar, maand, dag);

            CultureInfo belCI = new CultureInfo("nl-BE");

            string datum = mydate.ToString(belCI);
            
          

            Console.WriteLine($"{datum} is een {mydate.DayOfWeek}");

        }

        public static void Ticks2000Programma()
        {

            DateTime ticks2000 = new DateTime(2000, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan verschil = now - ticks2000;

            Console.WriteLine($"Sinds 1 januari 2000 zijn er {verschil.Ticks} ticks voorbijgegaan.");

        }

        public static void SchrikkeljaarProgramma()
        {
            int beginjaar = 1799;
            int eindjaar = 2021;
            int teller = 0;

            for (int i = beginjaar+1; i < eindjaar; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    teller++;
                }
            }
            Console.WriteLine($"Er zijn {teller} schrikkeljaren tussen 1799 en 2021");
        }

        public static void ArrayTimerProgramma()
        {
            DateTime tijd1 = DateTime.Now;
            int[] myArray = new int[1000000];

            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = i;
            }

            DateTime tijd2 = DateTime.Now;
            TimeSpan verschil = tijd2 - tijd1;

            Console.WriteLine($"Het duurt {verschil.TotalMilliseconds} miliseconden om een array van een miljoen elementen aan te maken en op te vullen " +
                $"met opeenvolgende waarden.");
        }

        public static void VerjaardagProgramma()
        {
            Console.WriteLine("Op welke dag ben je jarig?");
            int dag = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("In welke maand ben je jarig?");
            int maand = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Tot welk jaar te tellen?");
            int jaar = Convert.ToInt32(Console.ReadLine());


            DateTime verjaardag = new DateTime(jaar, maand, dag);
            DateTime nu = DateTime.Now;
            int teller = 0;
            //TimeSpan verschil = new TimeSpan();


            while (nu.Day != verjaardag.Day || nu.Month != verjaardag.Month || nu.Year != verjaardag.Year)
            {
                nu = nu.AddDays(1);
                teller++;
             
            }

            Console.WriteLine($"Nog {teller} dagen tot de volgende verjaardag");



        }
    }
}
