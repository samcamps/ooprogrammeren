﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen
{
    class KatMetCustomException
    {
        private byte leeftijd;

        public byte Leeftijd
        {
            get
            {
                return leeftijd;
            }

            private set
            {
                
                if (value > 25)
                {
                    throw new KatLeeftijdException(value,27,0);
                }
                this.leeftijd = value;
            }
        }


        public KatMetCustomException(byte leeftijd)
        {

            this.Leeftijd = leeftijd;

        }

    }
}
