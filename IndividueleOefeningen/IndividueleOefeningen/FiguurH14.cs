﻿using System;
namespace IndividueleOefeningen
{
    public abstract class FiguurH14: IComparable
    {
        //attributen
        private ConsoleColor kleur;


        //properties

        public ConsoleColor Kleur
        {
            get
            {
                return this.kleur;
            }
            set
            {
                this.kleur = value;
            }
        }

        //abstract property (dus met get; en/of set;)

        public abstract double Oppervlakte { get; }


        public int CompareTo(object obj)
        {

            if (!(obj is FiguurH14))
            {
                return 0;
            }
       

            if (this.Oppervlakte > ((FiguurH14)obj).Oppervlakte)
            {
                return 1;
            }

            else if (this.Oppervlakte == ((FiguurH14)obj).Oppervlakte)
            {
                return 0;
            }

            else
            {
                return -1;
            }


        }
    }
}
