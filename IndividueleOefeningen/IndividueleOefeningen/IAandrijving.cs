﻿using System;
namespace IndividueleOefeningen
{
    public interface IAandrijving
    {

        void EnergieToevoegen();
        void Vertragen(int kmPerUurPerSeconde, int doelsnelheid);
        void Versnellen(int kmPerUurPerSeconde, int doelsnelheid);


    }
}
