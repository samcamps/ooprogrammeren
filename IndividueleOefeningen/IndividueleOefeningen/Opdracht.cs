﻿using System;
namespace IndividueleOefeningen
{
    public class Opdracht
    {

        private static double btwTarief = 21.0;
        private double toegepasteBTW;
        private string omschrijving;


        public double BTWTarief
        {
            get
            {
                return this.BTWTarief;
            }
        }


        public double ToegepasteBTW
        {
            get
            {
                return this.toegepasteBTW;
            }
        }


        public string Omschrijving
        {
            get
            {
                return this.omschrijving;
            }
        }

        public Opdracht(string omschrijving)
        {
            this.omschrijving = omschrijving;
            this.toegepasteBTW = btwTarief;
        }


        public Opdracht(string omschrijving, double btw) : this(omschrijving)
        {
            this.toegepasteBTW = btw;
        }

        public void GeefWeer()
        {
            Console.WriteLine($"{this.Omschrijving}: {this.ToegepasteBTW:F0}% BTW");

        }

        public static void DemonstreeerOpdrachten()
        {
            Opdracht opdracht1 = new Opdracht("lek in de gootsteen");
            Opdracht opdracht2 = new Opdracht("dakpannen vervangen", 6);

            opdracht1.GeefWeer();
            opdracht2.GeefWeer();
        }




    }
}
