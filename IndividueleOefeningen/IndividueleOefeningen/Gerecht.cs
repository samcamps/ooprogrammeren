﻿using System;
namespace IndividueleOefeningen
{
    public class Gerecht
    {

    

        public string Naam
        {
            get; set;
        }

        public double Prijs { get; set; }


        public virtual void ToonOpMenu()
        {
            Console.WriteLine($"{this.Naam.PadRight(30)} {this.Prijs:F2}");
        }

        public Gerecht(string naam, double prijs)
        {
            this.Naam = naam;
            this.Prijs = prijs;
        }
    }
}
