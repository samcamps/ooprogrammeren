﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;


namespace IndividueleOefeningen
{
    abstract class Ketel
    {

        private static List<Ketel> alleKetels = new List<Ketel>();


        public static List<Ketel> AlleKetels
        {
            get
            {
                return alleKetels;

            }            
        }


        public int Inhoud
        {
            get;
            set;
        }

        public int Temperatuur
        {
            get;
            set;
        }


        public Ketel(int inhoud)
        {
            this.Inhoud = inhoud;
            alleKetels.Add(this);

        }


        public static List<Ketel> SelecteerKetels(List<Ketel> ketels, KetelFuncties ketelfunctie)
        {
            List<Ketel> temp = new List<Ketel>();

            switch (ketelfunctie)
            {


                case KetelFuncties.StoomVerwarmen:


                    foreach (var item in ketels)
                    {
                        if (item is IStoomVerwarmen)
                        {
                            temp.Add(item);
                        }

                    }
                    return temp;


                case KetelFuncties.Afkoelen:

                    foreach (var item in ketels)
                    {
                        if (item is IAfkoelen)
                        {
                            temp.Add(item);
                        }

                    }

                    return temp;


                case KetelFuncties.Verwarmen:

                    foreach (var item in ketels)
                    {
                        if (item is IVerwarmen)
                        {
                            temp.Add(item);
                        }

                    }
                    return temp;

                case KetelFuncties.WaterDoseren:

                    foreach (var item in ketels)
                    {
                        if (item is IWaterDoseren)
                        {
                            temp.Add(item);
                        }

                    }

                    return temp;


                default:
                    return null;

            }
        }


        public static List<Ketel> SelecteerKetelsVoorRecept(List<Ketel> ketels, List<KetelFuncties> recept)
        {

            foreach (var functie in recept)
            {

                ketels = SelecteerKetels(ketels, functie);
            }

            return ketels;

        }

        
    }
}
