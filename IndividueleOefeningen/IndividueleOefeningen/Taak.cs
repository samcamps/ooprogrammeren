﻿using System;
namespace IndividueleOefeningen
{
    public class Taak: IRoosterbaar
    {

        private TimeSpan werktijd;
        private string omschrijving;


        public Taak(TimeSpan werktijd, string omschrijving)
        {
            this.werktijd = werktijd;
            this.omschrijving = omschrijving;
        }

        public Taak()
        {

        }


        public TimeSpan Tijdsduur
        {
            get
            {
                return this.werktijd;
            }
        }

        public string Omschrijving
        {
            get
            {
                return this.omschrijving;
            }
        }

        public void Initialiseer()
        {
            Console.WriteLine("Omschrijving?");
            string omschrijving = Console.ReadLine();

            Console.WriteLine("Aantal minuten taak");
            int minutenTaak = Convert.ToInt32(Console.ReadLine());
            TimeSpan  duurtijdTaak = new TimeSpan(0, minutenTaak, 0);

            this.omschrijving = omschrijving;
            this.werktijd = duurtijdTaak;

        }


        public DateTime RoosterOm(DateTime referentiepunt)
        {
            return referentiepunt;
        }
    }
}
