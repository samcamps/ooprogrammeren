﻿using System;
namespace IndividueleOefeningen
{
    class KetelZonderDoseren:Ketel, IVerwarmen
    {
        public KetelZonderDoseren(int inhoud): base(inhoud)
        {
        }


        public void Verwarmen()
        {
            Console.WriteLine("KetelZonderDoseren - verwarmen");
        }
    }
}
