﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace IndividueleOefeningen
{
    public class DataStructuren
    {

        public static void ToonSubmenu()
        {
            Console.WriteLine("Uit te voeren oefening?\n\n1. Som per rij\n2. Som per kolom\n3. Pixels\n4. Boodschappenlijstje\n5. Kerstinkopen" +
                "\n6. Telefoonboek Naam-Nummer\n7. Telefoonboek Gemeente-Naam-Nummer\n8. Telefoonboek met builder");


            int keuze = Convert.ToInt32(Console.ReadLine());


            switch (keuze)
            {

                case 1:
                    SomPerRij();
                    break;

                case 2:
                    SomPerKolom();
                    break;

                case 3:
                    Pixels();
                    break;

                case 4:
                    H13Boodschappenlijstje();
                    break;

                case 5:
                    H13KerstInkopen();
                    break;

                case 6:
                    H13TelefoonboekNaamNummer();
                    break;

                case 7:
                    H13TelefoonboekGemeenteNaamNummer();
                    break;
                case 8:
                    H13TelefoonboekMetBuilder();
                    break;
            }

        }

        public static void SomPerRij()
        {

            Console.WriteLine("Hoeveel rijen telt je array?");
            int rijen = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hoeveel kolommen telt je array?");
            int kolommen = Convert.ToInt32(Console.ReadLine());

            int[,] myArray = new int[rijen, kolommen];

            for (int i = 0; i < rijen; i++)
            {

                for (int j = 0; j < kolommen; j++)
                {
                    Console.WriteLine($"Waarde voor rij {i + 1}, kolom {j + 1}?");
                    myArray[i, j] = Convert.ToInt32(Console.ReadLine());
                }

            }

            Console.WriteLine("Sommen per rij:");


            for (int i = 0; i < rijen; i++)
            {
                int result = 0;

                for (int j = 0; j < kolommen; j++)
                {
                    result = result + myArray[i, j];
                }
                Console.WriteLine($"Som rij {i + 1} is {result}");

            }
        }

        public static void SomPerKolom()
        {
            Console.WriteLine("Hoeveel rijen telt je array?");
            int rijen = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hoeveel kolommen telt je array?");
            int kolommen = Convert.ToInt32(Console.ReadLine());

            int[,] myArray = new int[rijen, kolommen];

            for (int i = 0; i < rijen; i++)
            {

                for (int j = 0; j < kolommen; j++)
                {
                    Console.WriteLine($"Waarde voor rij {i + 1}, kolom {j + 1}?");
                    myArray[i, j] = Convert.ToInt32(Console.ReadLine());
                }

            }

            Console.WriteLine("Sommen per kolom:");


            for (int i = 0; i < kolommen; i++)
            {
                int result = 0;

                for (int j = 0; j < rijen; j++)
                {
                    result = result + myArray[j, i];
                }
                Console.WriteLine($"Som kolom {i + 1} is {result}");
            }
        }

        public static void Pixels()
        {
            Console.WriteLine("Hoeveel pixels breed is uw afbeelding?");
            int rijen = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hoeveel pixels hoog is uw afbeelding?");
            int kolommen = Convert.ToInt32(Console.ReadLine());

            int[,] afbeelding = new int[rijen, kolommen];

            bool loop = true;

            while (loop)
            {
                Console.WriteLine($"Wat wil je doen?\n1. Een pixel kleuren\n2. De afbeelding tonen");
                int keuze = Convert.ToInt32(Console.ReadLine());

                if (keuze == 1)
                {

                    //een pixel een kleur geven

                    ConsoleColor[] kleuren = (ConsoleColor[])Enum.GetValues(typeof(ConsoleColor));

                    Console.WriteLine("Wat is de rij-index van de pixel (begin vanaf 0)?");
                    int rij = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Wat is de kolom-index van de pixel (begin vanaf 0)?");
                    int kolom = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Welke kleur wil je gebruiken?");

                    for (int i = 0; i < kleuren.Length; i++)
                    {
                        Console.WriteLine($"{(int)kleuren[i]} {kleuren[i]}");
                    }

                    int keuzekleur = Convert.ToInt32(Console.ReadLine());

                    afbeelding[rij, kolom] = keuzekleur;

                }

                else if (keuze == 2)
                {
                    // afbeelding to hiertoe weergeven

                    for (int i = 0; i < rijen; i++)
                    {
                        for (int j = 0; j < kolommen; j++)
                        {
                            Console.BackgroundColor = (ConsoleColor)afbeelding[i, j];
                            Console.Write($" ");
                            Console.ResetColor();
                        }
                        Console.WriteLine();
                    }
                }

                else
                {
                    loop = false;
                }
            }
        }

        public static void H13Boodschappenlijstje()
        {
            Console.WriteLine("We gaan de boodschappenlijst samenstellen");

            List<string> lijst = new List<string>();
            bool check = true;
            string answer;
            int i = 0;

            while (check)
            {

                Console.WriteLine($"Wat is item {i + 1} op je lijst? Geef een lege regel in om te stoppen.");
                answer = Console.ReadLine();

                if (answer == "")
                {
                    check = false;
                    Console.WriteLine();
                }
                else
                {
                    lijst.Add(answer);
                    i++;
                }

            }

            Console.WriteLine("Dit is je gesorteerde lijst");
            lijst.Sort();

            for (i = 0; i < lijst.Count; i++)
            {
                Console.WriteLine($"{i + 1}: {lijst[i]}");
            }
            Console.WriteLine("op naar de winkel!");


            do
            {
                Console.WriteLine("Welk item heb je gekocht? geef de naam exact zoals hij op je lijstje staat");
                string answerItem = Console.ReadLine();

                if (lijst.Contains(answerItem))
                {
                    lijst.Remove(answerItem);
                }
                else
                {
                    Console.WriteLine("Dit item bevindt zich niet op de lijst!");
                }


                Console.WriteLine("Nog winkelen? (Ja of Nee)");
                answer = Console.ReadLine().ToLower();

            } while (answer == "ja");

            Console.WriteLine("Naar huis met de boodschappen!");
            Console.WriteLine("Volgende items van je lijst ben je vergeten te kopen:");

            for (i = 0; i < lijst.Count; i++)
            {
                Console.WriteLine($"{lijst[i]}");
            }

        }


        public static void H13KerstInkopen()
        {
            double budget;
            Console.WriteLine("Wat is je budget voor je kerstinkopen?");
            budget = Convert.ToDouble(Console.ReadLine());
            double sum = 0;
            string input;
            double price;
            bool loop = true;
            int i = 0;
            List<double> prices = new List<double>();

            while (loop)
            {

                Console.WriteLine($"Wat is de prijs van cadeau {i + 1}?");
                input = Console.ReadLine();

                if (input != "")
                {
                    price = Convert.ToDouble(input);


                    prices.Add(price);
                    sum = sum + price;
                    i++;

                    if (sum > budget)
                    {
                        Console.WriteLine($"Je bent al {sum - budget:F1} over het budget!");
                    }
                }
                else
                {
                    loop = false;
                }

            }


            double gemiddelde = 0;
            foreach (double element in prices)
            {
                gemiddelde += element;
            }
            gemiddelde = gemiddelde / prices.Count;

            Console.WriteLine();
            Console.WriteLine("Info over je aankopen: ");
            Console.WriteLine($"Totaal bedrag: {sum:F1} euro.\nDuurste cadeau: {prices[prices.Count - 1]:F1} euro.\nGoedkoopste cadeau: {prices[0]:F1} euro." +
                $"\nGemiddelde prijs: {gemiddelde:F1} euro.");

        }

        public static void H13TelefoonboekNaamNummer()
        {
            Dictionary<string, string> telefoonboek = new Dictionary<string, string>();
            bool check = true;

            while (check)
            {

                Console.WriteLine("Wil je (nog) een naam en nummer inlezen?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "ja")
                {
                    Console.WriteLine("Naam?");
                    string key = Console.ReadLine();
                    Console.WriteLine("Nummer?");
                    string waarde = Console.ReadLine();

                    if (!telefoonboek.ContainsKey(key))
                    {
                        telefoonboek.Add(key, waarde);
                    }
                    else
                    {
                        telefoonboek[key] = waarde;
                    }

                }
                else
                {
                    check = false;
                }

            }

            foreach (var item in telefoonboek)
            {
                Console.WriteLine($"{item.Key}:\t {item.Value}");
            }


        }

        public static void H13TelefoonboekGemeenteNaamNummer()
        {

            Dictionary<string, Dictionary<string, string>> overzicht = new Dictionary<string, Dictionary<string, string>>();

            bool check = true;

            while (check)
            {

                Console.WriteLine("Wil je (nog) een gemeente, naam en nummer inlezen?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "ja")
                {
                    Console.WriteLine("Gemeente?");
                    string Gkey = Console.ReadLine();
                    Console.WriteLine("Naam?");
                    string Nkey = Console.ReadLine();
                    Console.WriteLine("Nummer?");
                    string Waarde = Console.ReadLine();

                    if (!overzicht.ContainsKey(Gkey))
                    {
                        overzicht.Add(Gkey, new Dictionary<string, string>());
                        overzicht[Gkey].Add(Nkey, Waarde);
                    }
                    else
                    {
                        overzicht[Gkey].Add(Nkey, Waarde);
                    }

                }
                else
                {
                    check = false;
                }
            }


            foreach (var element in overzicht)
            {
                Console.WriteLine($"{element.Key}: ");

                foreach (var item in element.Value)
                {
                    Console.WriteLine($"{item.Key}\t {item.Value}");
                }
            }
        }

        public static void H13TelefoonboekMetBuilder()
        {
            var builder = ImmutableDictionary.CreateBuilder<string, string>();

            bool check = true;

            while (check)
            {

                Console.WriteLine("Wil je (nog) een naam en nummer inlezen?");
                string answer = Console.ReadLine().ToLower();
                if (answer == "ja")
                {
                    Console.WriteLine("Naam?");
                    string key = Console.ReadLine();
                    Console.WriteLine("Nummer?");
                    string waarde = Console.ReadLine();

                    if (!builder.ContainsKey(key))
                    {
                        builder.Add(key, waarde);
                    }
                    else
                    {
                        builder[key] = waarde;
                    }

                }
                else
                {
                    check = false;
                }

            }

            ImmutableDictionary<string, string> telefoonboek = builder.ToImmutable();
            
            foreach (KeyValuePair<string,string> item in telefoonboek)
            {
                
                Console.WriteLine($"{item.Key}:\t {item.Value}");
            }
        }
    }
}
        

