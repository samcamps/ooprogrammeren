﻿using System;
namespace IndividueleOefeningen
{
    public class Auto
    {

        public string Autotype
        {
            get;
            set;
        }

        public IAandrijving Aandrijving
        {
            get;
            set;
        }


        public Auto(string autotype, IAandrijving aandrijving)
        {

            this.Autotype = autotype;
            this.Aandrijving = aandrijving;

        }
    }
}
