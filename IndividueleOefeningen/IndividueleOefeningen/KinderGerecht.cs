﻿using System;
namespace IndividueleOefeningen
{
    public class KinderGerecht:Gerecht
    {

        

        public override void ToonOpMenu()
        {
            Random mygen = new Random();
            int getal = mygen.Next(1, 16);

            Console.ForegroundColor = (ConsoleColor)getal;

            base.ToonOpMenu();

            Console.ResetColor();

         
        }

        
        public KinderGerecht(string naam, double prijs ) : base(naam,prijs)
        {
        }
    }
}
