﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen
{
    public class Polymorfisme
    {


        public static void ToonSubmenu()
        {
            Console.WriteLine("Welk onderwerp kies je?\n1. Autoconstructeur\n2. Grootkeuken\n3. Demonstreer IComparable<T>" +
                "\n4. Demonstreer IRoosterbaar\n5. Demonstreer Grootste (pizzza/fig)\n6. Demonstreer Kalender\n" +
                "7. Demonstreer Grootkeuken 2\n8. Demonstreer Grootkeuken 3");
            string answer = Console.ReadLine();

            switch (answer)
            {
                case "1":
                    DemonstreerAandrijving();
                    break;

                case "2":
                    DemonstreerGrootkeuken();
                    break;

                case "3":
                    DemonstreerVergelijkbareProducten();
                    break;

                case "4":
                    DemonstreerIRoosterbaar();
                    break;

                case "5":
                    DemonstreerGrootste();
                    break;


                case "6":
                    DemonstreerKalender1();
                    break;

                case "7":
                    DemonstreerGrootKeuken2();
                    break;

                case "8":
                    DemonstreerGrootKeuken3();
                    break;



                default:
                    break;
            }

        }
        
    

        public static void DemonstreerAandrijving()
        {

            Auto auto1 = new Auto("Benzine", new AandrijvingBenzine());
            auto1.Aandrijving.Versnellen(20, 50);
            auto1.Aandrijving = new AandrijvingElektrisch();
            auto1.Aandrijving.Versnellen(50, 80);

        }

        public static void DemonstreerGrootkeuken()
        {
            StoomKetel ketel1 = new StoomKetel(300);
            StoomKetel ketel2 = new StoomKetel(300);

            KetelZonderDoseren ketel3 = new KetelZonderDoseren(150);
            KetelZonderDoseren ketel4 = new KetelZonderDoseren(300);

            KetelMetDoseren ketel5 = new KetelMetDoseren(200);
            KetelMetDoseren ketel6 = new KetelMetDoseren(250);

            ketel1.StoomVerwarmen(100);
            ketel1.Afkoelen();
            ketel1.WaterDoseren();
            ketel5.WaterDoseren();

        }

        public static void DemonstreerVergelijkbareProducten()
        {
            var p1 = new Product("Fiets", 999);
            var p2 = new Product("Playstation 5", 500);
            var p3 = new Product("Elektrische gitaar", 750);
            var p4 = new Product("Doos ontbijtgranen", 3);
            Product p5 = null;
            var p6 = new Product("Xbox Series X", 500);
            var producten = new List<Product> { p1, p2, p3, p4, p5, p6 };
            producten.Sort();
            foreach (var p in producten)
            {
                if (!(p is null))
                {
                    Console.WriteLine($"{p.Naam}, {p.Kostprijs}");
                }
                else
                {
                    Console.WriteLine("NULL");
                }
            }

        }

        public static IComparable ZoekGrootste(List<IComparable> lijst)
        {
            IComparable grootste = lijst[0];

            foreach(IComparable item in lijst)
            {
                if (item.CompareTo(grootste) > 0)
                {
                    grootste = item;
                }

            }
            return grootste; 
        }

        public static void DemonstreerGrootste()
        {
            Pizza pizza3 = new Margarita (new string[] { "kaas", "champions", "kak" });
            Pizza pizza2 = new Margarita(new string[] { "kaas"});
            Pizza pizza1 = new Veggie (new string[] { "kaas", "champions", "kak" });

            List<Pizza> test = new List<Pizza>() { pizza1 };
            FiguurH14 fig1 = new VierkantH14(15);
            FiguurH14 fig2 = new VierkantH14(10);
            FiguurH14 fig3 = new VierkantH14(5);

            List<IComparable> lijstpizzas = new List<IComparable>() { pizza1, pizza2, pizza3};
            List<IComparable> lijstfiguren = new List<IComparable>() { fig1, fig2, fig3 };
            
            Console.WriteLine($"De duurste pizza is { ((Pizza)(ZoekGrootste(lijstpizzas))).Prijs}");
            Console.WriteLine($"De grootste figuur is { ((FiguurH14)(ZoekGrootste(lijstfiguren))).Oppervlakte}");
            

        }

        public static void DemonstreerIRoosterbaar()
        {
            IRoosterbaar blok1 = new Afspraak(new TimeSpan(0, 20, 0), new TimeSpan(1, 0, 0), new TimeSpan(0, 20, 0), "tandarts");
            IRoosterbaar blok2 = new Taak(new TimeSpan(2, 0, 0), "dagelijkse oefeningen OOP");
            Console.WriteLine($"Totale kalendertijd: {(blok1.Tijdsduur + blok2.Tijdsduur).Hours}u{(blok1.Tijdsduur + blok2.Tijdsduur).Minutes}m");

        }


        public static void DemonstreerKalender1()
        {
            Kalender kalender1 = new Kalender("DemonstratieKalender");
            kalender1.VoegToe();
            kalender1.ToonKalender();

        }

        public static void DemonstreerGrootKeuken2()
        {
            DemonstreerGrootkeuken();

            List<Ketel> onderhoudWaterDoseren = Ketel.SelecteerKetels(Ketel.AlleKetels, KetelFuncties.WaterDoseren);
            List<Ketel> onderhoudVerwarmen = Ketel.SelecteerKetels(Ketel.AlleKetels, KetelFuncties.Verwarmen);

            foreach(var item in onderhoudVerwarmen)
            {
                Console.WriteLine(item);
            }

            foreach (var item in onderhoudWaterDoseren)
            {
                Console.WriteLine(item);
            }
        }

        public static void DemonstreerGrootKeuken3()
        {
            DemonstreerGrootkeuken();
            List<KetelFuncties> recept1 = new List<KetelFuncties>() {KetelFuncties.WaterDoseren, KetelFuncties.Verwarmen };
            foreach(var item in Ketel.SelecteerKetelsVoorRecept(Ketel.AlleKetels, recept1))
            {
                
               
                Console.WriteLine(item.GetType().Name);
            }
                

        }

    }
}
