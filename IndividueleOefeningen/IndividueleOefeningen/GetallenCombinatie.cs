﻿using System;
namespace IndividueleOefeningen
{
    public class GetallenCombinatie
    {

        public int Getal1;
        public int Getal2;


        public double Som()
        {
            double result = this.Getal1 + this.Getal2;
            return result;
        }


        public double Verschil()
        {
            double result = this.Getal1 - this.Getal2;
            return result;

        }

        public double Product()
        {
            double result = this.Getal1 * this.Getal2;
            return result;

        }

        public double Quotient()
        {
            if (this.Getal2 == 0)
            {
                Console.WriteLine("Fout");
            }

            double result = (double) this.Getal1 / this.Getal2;
            return result;

        }

    }
}
