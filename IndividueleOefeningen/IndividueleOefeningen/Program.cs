﻿using System;

namespace IndividueleOefeningen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welkom bij de demo Objectgeorienteerd Programmeren!\nTopic van de uit te voeren oefening?\n");

           int keuze =-1;

            while (true && keuze != 0)
            {
                Console.WriteLine($"\n0 om te exitten\n1. DateTime\n2. Properties en access modifiers\n3. Recursie \n4. Exception Handling\n5. Polymorfisme");

                keuze = Convert.ToInt32(Console.ReadLine());


                switch (keuze)
                {
                    case 0:
                        //Console.WriteLine("Programma stopt...");
                        break;

                    case 1:
                        DateTimeOefeningen.ToonSubmenu();
                        break;


                    case 2:
                        EigenObjectOefeningen.ToonSubmenu();
                        break;

                    case 3:

                        Recursie.Recursie.ToonSubmenu();
                        break;

                    case 4:
                        ExceptionHandling.ToonSubmenu();
                        break;

                    case 5:
                        Polymorfisme.ToonSubmenu();
                        break;


                    default:
                        Console.WriteLine("Ongeldige keuze");
                        break;
                } 
            }
        }
    }
}
