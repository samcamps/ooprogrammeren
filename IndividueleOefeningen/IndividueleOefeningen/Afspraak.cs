﻿using System;
namespace IndividueleOefeningen
{
    public class Afspraak: IRoosterbaar
    {

        private TimeSpan duurtijdNaar;
        private TimeSpan duurtijdTerug;
        private TimeSpan duurtijdAfspraak;
        private string omschrijving;

        public Afspraak(TimeSpan duurtijdNaar, TimeSpan duurtijdAfspraak, TimeSpan duurtijdTerug,  string omschrijving)
        {
            this.duurtijdNaar = duurtijdNaar;
            this.duurtijdTerug = duurtijdTerug;
            this.duurtijdAfspraak = duurtijdAfspraak;
            this.omschrijving = omschrijving;
        }

        public Afspraak()
        {

        }


        public TimeSpan Tijdsduur
        {
            get
            {
                return (duurtijdNaar + duurtijdTerug + duurtijdAfspraak);
            }
        }


        public string Omschrijving
        {
            get
            {
                return this.omschrijving + " (inclusief verplaatsing)";
            }
        }

        public void Initialiseer()
        {
            Console.WriteLine("Omschrijving?");
            string omschrijving = Console.ReadLine();

            Console.WriteLine("Aantal minuten heen?");
            int minutenHeen = Convert.ToInt32(Console.ReadLine());
            TimeSpan minutenHeenTS = new TimeSpan(0, minutenHeen, 0);

            Console.WriteLine("Aantal minuten terug?");
            int minutenTerug = Convert.ToInt32(Console.ReadLine());
            TimeSpan minutenTerugTS = new TimeSpan(0, minutenTerug, 0);

            Console.WriteLine("Aantal minuten afspraak zelf");
            int minutenAfspraak = Convert.ToInt32(Console.ReadLine());
            TimeSpan minutenAfspraakTS = new TimeSpan(0, minutenAfspraak, 0);

            this.omschrijving = omschrijving;
            this.duurtijdNaar = minutenHeenTS;
            this.duurtijdTerug = minutenTerugTS;
            this.duurtijdAfspraak = minutenAfspraakTS;

        }


        public DateTime RoosterOm(DateTime referentiepunt)
        {
            return referentiepunt - this.duurtijdNaar;

        }
    }
}
