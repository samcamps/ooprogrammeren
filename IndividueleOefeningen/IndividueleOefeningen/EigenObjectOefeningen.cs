﻿using System;
namespace IndividueleOefeningen
{
    public class EigenObjectOefeningen
    {

        public static void ToonSubmenu()
        {
            Console.WriteLine("Uit te voeren oefening?\n\n1. Demonstreer Operaties\n2. Demonstreer Figuren\n3. Demonstreer Figuren met constructor" +
                "\n4. Demonstreer Dobbelsteen\n5. Demonstreer Opdrachten\n6. Demonstreer Datastructuren\n7. Demonstreer figuren met overerving" +
                "\n8. Demonstreer Bestelling\n9. Demonstreer Pizzas\n10. Demonstreer Gerechten\n11. Demonstreer Vergelijkbare figuren");


            int keuze = Convert.ToInt32(Console.ReadLine());


            switch (keuze)
            {
               
                case 1:
                    DemonstreerOperaties();
                    break;

                case 2:
                    DemonstreerFiguren();
                    break;

                case 3:
                    DemonstreerFigurenMetConstructor();
                    break;

                case 4:
                    Dobbelsteen.DemonstreerDobbelsteen();
                    break;

                case 5:
                    Opdracht.DemonstreeerOpdrachten();
                    break;

                case 6:
                    DataStructuren.ToonSubmenu();
                    break;

                case 7:
                    DemonstreerFigurenOvererving();
                    break;
                case 8:
                    DemonstreerBestellingen();
                    break;

                case 9:
                    DemonstreerPizzas();
                    break;

                case 10:
                    DemonstreerGerechten();
                    break;

                case 11:
                    DemonstreerVergelijkbareFiguren();
                    break;



                default:
                    Console.WriteLine("Dit is geen geldige keuze");
                    break;
            }
        }


        public static void DemonstreerOperaties()
        {

            GetallenCombinatie paar1 = new GetallenCombinatie();
            paar1.Getal1 = 12;
            paar1.Getal2 = 34;
            Console.WriteLine("Paar:" + paar1.Getal1 + ", " + paar1.Getal2);
            Console.WriteLine("Som = " + paar1.Som());
            Console.WriteLine("Verschil = " + paar1.Verschil());
            Console.WriteLine("Product = " + paar1.Product());
            Console.WriteLine("Quotient = " + paar1.Quotient());
        }


        public static void DemonstreerFiguren()
        {
            Rechthoek rechthoek1 = new Rechthoek();
            rechthoek1.Breedte = -1;
            rechthoek1.Breedte = 0;

            Rechthoek rechthoek2 = new Rechthoek();
            rechthoek2.Breedte = 2.2;
            rechthoek2.Hoogte = 1.5;


            Rechthoek rechthoek3 = new Rechthoek();
            rechthoek3.Breedte = 3;
            rechthoek3.Hoogte = 1;

            Driehoek driehoek1 = new Driehoek();
            driehoek1.Basis = 3;
            driehoek1.Hoogte = 1;

            Driehoek driehoek2 = new Driehoek();
            driehoek2.Basis = 2;
            driehoek2.Hoogte = 2;

            Console.WriteLine(rechthoek2.Oppervlakte);
            Console.WriteLine(rechthoek3.Oppervlakte);
            Console.WriteLine(driehoek1.Oppervlakte);
            Console.WriteLine(driehoek2.Oppervlakte);

        }

        public static void DemonstreerFigurenMetConstructor()
        {
            Rechthoek rechthoek1 = new Rechthoek(-1,0);
            
            Rechthoek rechthoek2 = new Rechthoek(1.5,2.2);
            Rechthoek rechthoek3 = new Rechthoek(1,3);
    
            Driehoek driehoek1 = new Driehoek(1,3);
            Driehoek driehoek2 = new Driehoek(2,2);
          
            Console.WriteLine(rechthoek2.Oppervlakte);
            Console.WriteLine(rechthoek3.Oppervlakte);
            Console.WriteLine(driehoek1.Oppervlakte);
            Console.WriteLine(driehoek2.Oppervlakte);

        }


        public static void DemonstreerFigurenOvererving()
        {
                ParallellogramH14 parallellogram = new ParallellogramH14(6, 2);
                parallellogram.Kleur = ConsoleColor.Cyan;
                Console.WriteLine($"De oppervlakte van het parallellogram met basis {parallellogram.Basis} en hoogte {parallellogram.Hoogte} is {parallellogram.Oppervlakte}.");

                RechtHoekH14 rechthoek = new RechtHoekH14(6, 3);
                rechthoek.Lengte = 5;
                Console.WriteLine($"De oppervlakte van de rechthoek met lengte {rechthoek.Basis} en breedte {rechthoek.Hoogte} is {rechthoek.Oppervlakte}.");

                VierkantH14 vierkant1 = new VierkantH14(5);
                vierkant1.Zijde = 3;
                Console.WriteLine($"De oppervlakte van het vierkant met zijde {vierkant1.Breedte} is {vierkant1.Oppervlakte}.");

                RuitH14 ruit = new RuitH14(4, 5);
                Console.WriteLine($"De oppervlakte van de ruit met diagonalen {ruit.Diagonaal1} en {ruit.Diagonaal2} is {ruit.Oppervlakte}.");

                FiguurH14 cirkel = new CirkelH14(6);
                Console.WriteLine($"De oppervlakte van de cirkel is {cirkel.Oppervlakte:F2}.");

                FiguurH14 driehoek = new DriehoekH14(3, 11);
                Console.WriteLine($"De oppervlakte van de driehoek is {driehoek.Oppervlakte}.");
           //  driehoek.Kleur = ConsoleColor.Blue;
        }

        public static void DemonstreerBestellingen()
        {
            
            Console.WriteLine("Hoeveel stuks wil je bestellen?");
            uint aantal = Convert.ToUInt32(Console.ReadLine());
            Console.WriteLine("Wat is de basiprijs?");
            double basisprijs = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Wil je een gewone (1) of een internationale (2) bestelling doen?");
            int antwoord = Convert.ToInt32(Console.ReadLine());

            Bestelling order1=null;

            if (antwoord ==1)

            {
               order1 = new Bestelling(aantal, basisprijs);
                
            }

            else if (antwoord == 2)
            {
              order1 = new InternationaleBestelling (aantal, basisprijs);
              
            }

            Console.WriteLine($"Totaalprijs is: {order1.TotaalPrijs}");



        }

        public static void DemonstreerPizzas()
        {
            Pizza pizza1 = new Margarita();
            Pizza pizza2 = new Veggie();

            Console.WriteLine($"Een margarita zonder extra's kost: {pizza1.BasisPrijs}\n" +
                $"De ingredienten zijn:\n");
            pizza1.ToonIngredienten();

            Console.WriteLine($"Een veggie zonder extra's kost: {pizza2.BasisPrijs}\n" +
                $"De ingredienten zijn:\n");
            pizza2.ToonIngredienten();

        }

        public static void DemonstreerGerechten()
        {
            Gerecht item1 = new Gerecht("Paling in 't groen", 22.00);
            Gerecht item2 = new KinderGerecht("Vol-au-vent", 11.00);
            Gerecht item3 = new Gerecht("Waterzooi", 22.00);
            Gerecht item4 = new KinderGerecht("Kabouterschnitzel", 12.00);

            item1.ToonOpMenu();
            item2.ToonOpMenu();
            item3.ToonOpMenu();
            item4.ToonOpMenu();

        }

        public static void DemonstreerVergelijkbareFiguren()
        {

            VergelijkbareCirkel fig1 = new VergelijkbareCirkel(5);
            VergelijkbareCirkel fig2 = new VergelijkbareCirkel(5);
            VergelijkbareCirkel fig3 = new VergelijkbareCirkel(10);

            VergelijkbareDriehoek fig4 = new VergelijkbareDriehoek(2, 5);
            VergelijkbareDriehoek fig5 = new VergelijkbareDriehoek(2, 5);
            VergelijkbareDriehoek fig6 = new VergelijkbareDriehoek(3, 5);

            VergelijkbareParallellogram fig7 = new VergelijkbareParallellogram(2, 9);
            VergelijkbareParallellogram fig8 = new VergelijkbareParallellogram(2, 9);
            VergelijkbareParallellogram fig9 = new VergelijkbareParallellogram(3, 9);

            VergelijkbareRechthoek fig10 = new VergelijkbareRechthoek( 20.8, 5);
            VergelijkbareRechthoek fig11 = new VergelijkbareRechthoek(5, 6);
            VergelijkbareRechthoek fig12 = new VergelijkbareRechthoek(3,6);


            VergelijkbareVierkant fig13 = new VergelijkbareVierkant(5);
            VergelijkbareVierkant fig14 = new VergelijkbareVierkant(6);


            VergelijkbareRuit fig15 = new VergelijkbareRuit(5,10);
            VergelijkbareRuit fig16 = new VergelijkbareRuit(3,6);


            Console.WriteLine(fig1.Equals(fig2));
            Console.WriteLine(fig2.Equals(fig3));
            Console.WriteLine(fig1.Equals(fig1));
            Console.WriteLine();
            Console.WriteLine(fig4.Equals(fig5));
            Console.WriteLine(fig5.Equals(fig6));
            Console.WriteLine(fig6.Equals(fig6));
            Console.WriteLine();
            Console.WriteLine(fig7.Equals(fig8));
            Console.WriteLine(fig8.Equals(fig9));
            Console.WriteLine(fig7.Equals(fig7));
            Console.WriteLine();
            Console.WriteLine(fig10.Equals(fig11));
            Console.WriteLine(fig11.Equals(fig12));
            Console.WriteLine(fig12.Equals(fig12));
            Console.WriteLine();
            Console.WriteLine(fig1.Equals(fig6));

            Console.WriteLine();
            Console.WriteLine(fig13.Equals(fig14));

            Console.WriteLine();
            Console.WriteLine(fig15.Equals(fig15));


            fig15.Diagonaal1 = 9;
            fig15.Diagonaal2 = 4;
            Console.WriteLine($"dia1 = {fig15.Diagonaal1} dia2 = {fig15.Diagonaal2} maar Basis = {fig15.Basis} en Hoogte is={fig15.Hoogte} en oppervlakte =  {fig15.Oppervlakte}");

            Console.WriteLine(fig1);
            Console.WriteLine(fig4);
            Console.WriteLine(fig7);
            Console.WriteLine(fig10);

            Console.WriteLine(fig13);
            Console.WriteLine(fig15);
            Console.WriteLine(fig14.Oppervlakte);

        }



    }

}

