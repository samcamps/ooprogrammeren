﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace IndividueleOefeningen
{
    class Product : IComparable<Product>
    {

            private uint kostprijs;
            public uint Kostprijs
            {
                get { return kostprijs; }
                set { kostprijs = value; }
            }

            private string naam;
            public string Naam
            {
                get { return naam; }
                set { naam = value; }
            }
       
    
    public Product(string naam, uint kostprijs)
        {
            this.Naam = naam;
            this.Kostprijs = kostprijs;
        }

        public int CompareTo([AllowNull]Product other)
        {
            if(other is null)
            {
                return 1;
            }

            if (this.Kostprijs > other.Kostprijs)
            {
                return 1;

            }

            if (this.Kostprijs == other.Kostprijs)
            {
                return 0;
            }
            
            else
            {
                return -1;
            }

            
        }
    }
}
