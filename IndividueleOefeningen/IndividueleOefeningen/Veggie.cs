﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen
{
    class Veggie : Pizza
    {


        public override double BasisPrijs
        {
            get
            {
                return 6;
            }

        }


        public Veggie(string[] extratop) : base(extratop)
        {
            this.ingredienten.Remove("kaas");
            this.ingredienten.Add("tofu");
            this.ingredienten.Add("spinazie");
        }

        public Veggie () : base(new string[] {"tofu", "spinazie" })
        {
            this.ingredienten.Remove("kaas");
        }

    }
}
