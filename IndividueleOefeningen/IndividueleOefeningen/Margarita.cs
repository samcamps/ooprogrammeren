﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen
{
    class Margarita : Pizza
    {


        public override double BasisPrijs
        {
            get
            {
                return 5;
            }

        }



        public Margarita(string[] extratop) : base (extratop)
        {
            this.ingredienten.Add("mozarella");
        }

        public Margarita() : base (new string[] { "mozarella" })
        {

        }
    }
}


