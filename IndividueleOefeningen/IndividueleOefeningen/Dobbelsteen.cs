﻿using System;
namespace IndividueleOefeningen
{
    public class Dobbelsteen
    {

        private uint aantalZijden;
        private static Random generator = new Random();


        public Dobbelsteen(uint aantalzijden)
        {
            this.aantalZijden = aantalzijden;
        }

        public Dobbelsteen() : this(6)
        {

        }


        public void Werp()
        {
            Console.WriteLine(generator.Next(0, (int)this.aantalZijden));
        }


        public static void DemonstreerDobbelsteen()
        {
            Dobbelsteen d1 = new Dobbelsteen();
            Dobbelsteen d2 = new Dobbelsteen(20);

            d1.Werp();
            d1.Werp();
            d1.Werp();
            d2.Werp();
            d2.Werp();
            d2.Werp();
        }
    }
}
