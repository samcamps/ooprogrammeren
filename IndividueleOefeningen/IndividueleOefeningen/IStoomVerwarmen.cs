﻿using System;
namespace IndividueleOefeningen
{
    public interface IStoomVerwarmen
    {
        void StoomVerwarmen(int temperatuur);
    }
}
