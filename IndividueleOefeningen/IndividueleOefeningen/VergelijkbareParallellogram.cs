﻿using System;
namespace IndividueleOefeningen
{
    public class VergelijkbareParallellogram : VergelijkbareFiguur
    {

        private double basis;
        private double hoogte;


        public double Basis 
        {
            get
            {
                return this.basis;
            }
            protected set
            {
                this.basis = value;
            }
        }


        public double Hoogte
        {
            get
            {
            return this.hoogte;
            }
            protected set
            {
            this.hoogte = value;
            }
        }


        public override double Oppervlakte
        {
            get
            {
                return (this.Hoogte * this.Basis);
            }
             
        }


        public VergelijkbareParallellogram(double basis, double hoogte)
        {
            this.Basis = basis;
            this.Hoogte = hoogte;
        }



        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            
            else
            {
                VergelijkbareParallellogram temp = (VergelijkbareParallellogram) obj;
                //VergelijkbareParallellogram temp2 = (VergelijkbareParallellogram)this;
                if(this.Basis == temp.Basis && this.Hoogte == temp.Hoogte) { return true; }
                else { return false; }

            }

        }

        public override int GetHashCode()
        {
            return this.Basis.GetHashCode() + this.Hoogte.GetHashCode();
        }

        public override string ToString()
        {
            return $"Dit is een object van klasse {this.GetType().Name} met basis {this.Basis} en hoogte {this.Hoogte}";
        }

    }
}
