﻿using System;
namespace IndividueleOefeningen
{
    public class CirkelH14 : FiguurH14
    {

        private double straal;


        public double Straal
        {
            get
            {
                return this.straal;
            }
            set
            {
                this.straal = value;
            }
        }

        public override double Oppervlakte
        {
            get
            {
                return Math.Pow(this.Straal, 2) * Math.PI;
            }
          
        }


        public CirkelH14(double straal)
        {
            this.Straal = straal;
        }

    }
}
