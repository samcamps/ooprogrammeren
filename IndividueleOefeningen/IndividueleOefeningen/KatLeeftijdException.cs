﻿using System;
namespace IndividueleOefeningen
{
    public class KatLeeftijdException : ArgumentException
    {

        private byte meegegevenWaarde;
        private byte laagstMogelijkeWaarde;
        private byte hoogstMogelijkeWaarde;

        public byte MeegegevenWaarde
        {
            get
            {
                return this.meegegevenWaarde;
            }

        }


        public byte LaagstMogelijkeWaarde
        {
            get
            {
                return this.laagstMogelijkeWaarde;
            }
        }


        public byte HoogstMogelijkeWaarde
        {
            get
            {
                return this.hoogstMogelijkeWaarde;
            }
        }
      


        public KatLeeftijdException(byte meegegeven, byte hoogste, byte laagste)
        {
            this.meegegevenWaarde = meegegeven;
            this.hoogstMogelijkeWaarde = hoogste;
            this.laagstMogelijkeWaarde = laagste;

        }
    }
}
