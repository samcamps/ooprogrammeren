﻿using System;
namespace IndividueleOefeningen
{
    class StoomKetel:Ketel, IAfkoelen, IStoomVerwarmen, IWaterDoseren
    {
        public StoomKetel(int inhoud) :base(inhoud)
        {
        }


        public void Afkoelen()
        {
            Console.WriteLine("Stoomketel - afkoelen");
        }


        public void StoomVerwarmen(int temperatuur)
        {
            Console.WriteLine($"Stoomketel - stoom verwarmen tot {temperatuur}");
        }

        public void WaterDoseren()
        {
            Console.WriteLine("Stoomketel - water doseren");
        }



    }
}
