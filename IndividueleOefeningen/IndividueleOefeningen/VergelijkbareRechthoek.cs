﻿using System;
namespace IndividueleOefeningen
{
    public class VergelijkbareRechthoek:VergelijkbareParallellogram
    {

        public double Breedte
        {
            get
            {
                return Hoogte;
            }

            protected set
            {
                Hoogte = value;
            }
        }

        public double Lengte
        {
            get
            {
                return Basis;
            }

            protected set
            {
                Basis = value;
            }
        }

      

        public VergelijkbareRechthoek() : base (0,0)
        {
        }

        public VergelijkbareRechthoek(double breedte, double lengte) : base(lengte, breedte)
        {
        }

        public override string ToString()
        {
            string result="";
            
            for (int j = 0; j < Math.Ceiling(this.Lengte); j++)
            {
                for (int i = 0; i < Math.Ceiling(this.Breedte); i++)
                {
                    result = result + ".";

                }
                result = result + "\n";
            }

            return result;
        }

        

    }
}
