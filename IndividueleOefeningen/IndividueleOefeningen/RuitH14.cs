﻿using System;
namespace IndividueleOefeningen
{
    public class RuitH14 : ParallellogramH14
    {

     private double diagonaal1;
     private double diagonaal2;

      public double Diagonaal1
        {
            get
            {
                return this.diagonaal1;
            }
            set
            {
                this.diagonaal1 = value;
            }
        }


        public double Diagonaal2
        {
            get
            {
                return this.diagonaal2;
            }
            set
            {
                this.diagonaal2 = value;
            }
        }

        public override double Oppervlakte
        {
            get
            {
                return (this.Diagonaal1 * this.Diagonaal2) / 2;
            }
        }




        public RuitH14(double diagonaal1, double diagonaal2) : base(diagonaal1, diagonaal2)
        {
            this.Diagonaal1 = diagonaal1;
            this.Diagonaal2 = diagonaal2;
        }

    }
}

