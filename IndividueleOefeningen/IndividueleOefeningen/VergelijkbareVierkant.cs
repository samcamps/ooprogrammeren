﻿using System;
namespace IndividueleOefeningen
{
    public class VergelijkbareVierkant:VergelijkbareRechthoek
    {

        public double Zijde
        {
            get
            {
                return Basis;
            }
            private set
            {
                Basis = value;
                Hoogte = value;
            }
        }



        public VergelijkbareVierkant(double zijde):base(zijde,zijde)
        {
            
        }
    }
}
