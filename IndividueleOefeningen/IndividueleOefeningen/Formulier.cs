﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace IndividueleOefeningen
{
    class Formulier
    {

        private List<FormulierVraag> vragen;

        public ImmutableList<FormulierVraag> Vragen
        {
            get
            {
                return vragen.ToImmutableList<FormulierVraag>();
            }
        }


        public void VulIn()
        {
            try
            {
                foreach(var item in Vragen)
                {
                    item.Toonvraag();
                    item.LeesAntwoord();

                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Onverwachte fout wordt naar schijf weggeschreven");
                throw ex;
            }
            

        }

        public void Toon()
        {
            foreach (var item in Vragen)
            {
                Console.WriteLine(item.Tekst);
                Console.WriteLine(item.Antwoord);
            }

        }

        public Formulier(List<FormulierVraag> vragen)
        {
            this.vragen = new List<FormulierVraag>();
            foreach (var vraag in vragen)
            {
                this.vragen.Add(vraag);
            }
        }    
    }
}
