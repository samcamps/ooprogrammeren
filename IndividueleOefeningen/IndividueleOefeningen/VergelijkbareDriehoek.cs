﻿using System;
namespace IndividueleOefeningen
{
    public class VergelijkbareDriehoek : VergelijkbareFiguur
    {

        private double basis;
        private double hoogte;
        

        public double Basis
        {
            get
            {
                return this.basis;
            }
            private set
            {
                this.basis = value;
            }
        }


        public double Hoogte
        {
            get
            {
                return this.hoogte;
            }
            private set
            {
                this.hoogte = value;
            }
        }


        public override double Oppervlakte
        {
            get
            {
                return (this.Hoogte * this.Basis) / 2;
            }
        }


        public VergelijkbareDriehoek(double basis, double hoogte)
        {
            this.Basis = basis;
            this.Hoogte = hoogte;
        }

        public VergelijkbareDriehoek()
        {

        }

        public override bool Equals(object obj)
        {

            if (obj is null)
            {
                return false;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            else
            {
                VergelijkbareDriehoek temp = (VergelijkbareDriehoek)obj;
                if(this.Basis == temp.Basis && this.Hoogte == temp.Hoogte)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


        public override string ToString()
        {
            return$"Dit is een object van klasse {this.GetType().Name} met basis {this.Basis} en hoogte {this.Hoogte}";
        }
        
    }
}
