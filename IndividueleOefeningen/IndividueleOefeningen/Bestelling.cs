﻿using System;
namespace IndividueleOefeningen
{
    public class Bestelling
    {
    
        protected double basisPrijs;
        private uint aantal;


        public uint Aantal
        {
            get
            {
                return aantal;
            }
            set
            {
                aantal = value;
            }
                    
        }

        public virtual double TotaalPrijs
        {
            get
            {
                return Aantal * basisPrijs;
            }
          
        }




        public Bestelling(uint aantal, double basisprijs)
        {
            this.Aantal = aantal;
            this.basisPrijs = basisprijs;
        }
    }
}
