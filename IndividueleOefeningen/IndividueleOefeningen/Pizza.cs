﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen
{

    abstract class Pizza:IComparable
    {
        protected List<string> ingredienten;

        public Pizza(string[] extraToppings)
        {
            this.ingredienten = new List<string> { "deeg", "tomatensaus", "kaas" };
            foreach (var topping in extraToppings)
            {
                this.ingredienten.Add(topping);
            }
        }

        public abstract double BasisPrijs
        {
            get;
        }

        public double Prijs
        {
            get
            {
                return this.BasisPrijs + (this.ingredienten.Count * 0.5);
            }
        }

        public int CompareTo(object obj)
        {

            if (!(obj is Pizza))
            {
                return 0;
            }

            else
            {
                Pizza temp = (Pizza)obj;
                if (this.Prijs > temp.Prijs)
                {
                    return 1;
                }
                else if (this.Prijs == temp.Prijs)
                {
                    return 0;
                }

                else
                {
                    return -1;
                }
            }

        }

        public void ToonIngredienten()
        {
            foreach (var ingredient in ingredienten)
            {
                Console.WriteLine(ingredient);
            }
        }


    }
}