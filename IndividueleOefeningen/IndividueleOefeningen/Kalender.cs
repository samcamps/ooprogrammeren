﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;

namespace IndividueleOefeningen
{
    public class Kalender
    {

        private string naam;
        private Dictionary<DateTime, IRoosterbaar> kalenderooster = new Dictionary<DateTime, IRoosterbaar>();


        public Kalender(string naam)
        {
            this.naam = naam;
        }



        public void VoegToe()
        {
            bool cont = true;

            while (cont)
            {
                Console.WriteLine(DateTime.Now);
                Console.WriteLine("Om wat voor object gaat het?\n1. Afspraak\n2. Taak\n3. Stoppen");
                int answer = Convert.ToInt32(Console.ReadLine());
                IRoosterbaar item;

                if (answer == 1)
                {

                    item = new Afspraak();

                }

                else if (answer == 2)
                {

                    item = new Taak();

                }

                else
                {
                    break;
                }


                item.Initialiseer();
                Console.WriteLine("Wanneer moet dit geroosterd worden?");
                DateTime tijdstip = Convert.ToDateTime(Console.ReadLine());

                this.kalenderooster.Add(item.RoosterOm(tijdstip), item);
            }
        }


        public void ToonKalender()
        {
     
            foreach(var item in kalenderooster)
            {
                Console.WriteLine($"{item.Key}: {item.Value.Omschrijving}");
            }
        }        

    }
}
