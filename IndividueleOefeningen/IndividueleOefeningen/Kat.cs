﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen
{
    class Kat
    {
        private byte leeftijd;

        public byte Leeftijd
        {
            get
            {
                return Leeftijd;
            }

            private set
            {
                if (value > 25)
                {
                    throw new ArgumentException("Kat is te oud");
                }
                this.leeftijd = value;
            }
        }


        public Kat(byte leeftijd)
        {
            
          this.Leeftijd = leeftijd;
          Console.WriteLine("Kat werd succevol aangemaakt");

        }



        public static void DemonstreerLeeftijdKatMetResourceCleanup()
        {
            List<Kat> lijstkatten = new List<Kat>();
            Random myrand = new Random();
            byte randomleeftijd;


            try
            {
                for (int i = 0; i < 20; i++)
                {
                    randomleeftijd = (byte)myrand.Next(0, 31);
                    Console.WriteLine(randomleeftijd);
                    lijstkatten.Add(new Kat(randomleeftijd));
                }

                Console.WriteLine("De volledige lijst met katten is aangemaakt");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Het is niet gelukt :-(");
            }

            finally
            {
                lijstkatten.Clear();
            }

                     

        }
           


    }
}
