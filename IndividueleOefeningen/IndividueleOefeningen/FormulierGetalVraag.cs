﻿using System;
namespace IndividueleOefeningen
{
    class FormulierGetalVraag:FormulierVraag
    {

        private int onderGrens;
        private int bovenGrens;

        public int OnderGrens
        {
            get
            {
                return onderGrens;
            }
            set
            {
                onderGrens = value;
            }
        }

        public int BovenGrens
        {
            get
            {
                return bovenGrens;
            }
            set
            {
                bovenGrens = value;
            }
        }


        public override void Toonvraag()
        {
            Console.WriteLine($"{this.Tekst}");
            Console.WriteLine($"Dit zou een getal moeten zijn tussen {this.OnderGrens} en {this.BovenGrens}.");
        }



        public override void LeesAntwoord()
        {
            bool cont = true;
            string input;
            int temp;

            while (cont)
            {

                input = Console.ReadLine();
                temp = Convert.ToInt32(input);

                if (temp > this.OnderGrens && temp < this.BovenGrens)
                {
                    this.Antwoord = input;
                    cont = false;
                }

                else
                {
                    Console.WriteLine($"Je antwoord moet tussen {this.OnderGrens} en {this.BovenGrens} liggen");
                }

            }
           
        }


        public FormulierGetalVraag(string tekst, int onderGrens, int bovenGrens):base(tekst)
        {
            if (onderGrens > bovenGrens)
            {
                throw new ArgumentException("Ondergrens is groter dan de bovengrens");
            }
            this.OnderGrens = onderGrens;
            this.BovenGrens = bovenGrens;
        }
    }
}
