﻿using System;
namespace IndividueleOefeningen
{
    public class VergelijkbareCirkel : VergelijkbareFiguur
    {

        private double straal;


        public double Straal
        {
            get
            {
                return this.straal;
            }
            private set
            {
                this.straal = value;
            }
        }

        public override double Oppervlakte
        {
            get
            {
                return Math.Pow(this.Straal, 2) * Math.PI;
            }
          
        }


        public VergelijkbareCirkel(double straal)
        {
            this.Straal = straal;
        }



        public override bool Equals(object obj)
        {
            
            if (obj is null)
            {
                return false;
            }

            bool gelijk;
            if (this.GetType() != obj.GetType())
            {
                gelijk = false;
            }

            else
            {
                VergelijkbareCirkel temp = (VergelijkbareCirkel)obj;
                if(this.Straal == temp.Straal)
                {
                    gelijk = true;
                }
                else
                {
                    gelijk = false;
                }

            }

            return gelijk;
                
        }

        public override int GetHashCode()
        {
            return this.Straal.GetHashCode();
            
        }
         

        public override string ToString()
        {
            return $"Dit is een object van klasse {this.GetType().Name} met straal {this.Straal}";
        }

    }
}
