﻿using System;
namespace IndividueleOefeningen
{
    public class ParallellogramH14 : FiguurH14
    {

        private double basis;
        private double hoogte;


        public double Basis 
        {
            get
            {
                return this.basis;
            }
            set
            {
                this.basis = value;
            }
        }


        public double Hoogte
        {
            get
            {
            return this.hoogte;
            }
            set
            {
            this.hoogte = value;
            }
        }


        public override double Oppervlakte
        {
            get
            {
                return (this.Hoogte * this.Basis);
            }
             
        }


        public ParallellogramH14(double basis, double hoogte)
        {
            this.Basis = basis;
            this.Hoogte = hoogte;
        }

    }
}
