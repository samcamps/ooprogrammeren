﻿using System;
using System.IO;

namespace IndividueleOefeningen.Recursie
{
    public class Recursie
    {

        public static void ToonSubmenu()
        {
            Console.WriteLine($"1. Demonstreer Faculteiten\n2. Tel CS-bestanden\n3. Contact Tracing");
            string answer = Console.ReadLine();

            switch (answer)
            {
                case "1":
                    DemonstreerRecursie();
                    break;

                case "2":
                    TelCSBestanden();
                    break;

                case "3":
                    Person.SetupTracing();
                    break;

                default:
                    break;
            }
        }

        public static ulong Faculteit(byte n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return Faculteit((byte)(n - 1)) * n;
            }

        }

        public static void DemonstreerRecursie()
        {
            Console.WriteLine($"5! = {Faculteit(5)}");
            Console.WriteLine($"8! = {Faculteit(8)}");
            Console.WriteLine($"20! = {Faculteit(20)}");
        }

        public static void TelCSBestanden()
        {
            Console.WriteLine("Wat is de rootmap van de .cs bestanden");
            string root = Console.ReadLine();
            DirectoryInfo di = new DirectoryInfo(root);
            Console.WriteLine($"Je hebt {TotalCSbytes(di)/1024:F2} KB aan .cs bestanden");

        }

        public static long TotalCSbytes(DirectoryInfo root)
        {
            long totalSize = 0;

            foreach (FileInfo fi in root.EnumerateFiles())
            {
                if (fi.Extension ==".cs")
                {
                    //Console.WriteLine(fi.Name);
                    totalSize = totalSize + fi.Length;
                }
              
            }

            
            foreach (DirectoryInfo di in root.EnumerateDirectories())
            {
               totalSize = totalSize + TotalCSbytes(di);
            }

            return totalSize;

        }
        //Users/samcamps/Documents/AP20-21/OOProgrammeren
        
    }


}
