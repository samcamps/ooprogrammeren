﻿using System;
using System.Collections.Generic;

namespace IndividueleOefeningen.Recursie
{
    public class Person
    {


        public string Naam
        {
            get;
            private set;
        }

        public HashSet<Person> Contact
        {
            get;
            set;

        }



        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;

            }

            if (obj is Person)
            {
                Person temp = (Person)obj;
                if (this.Naam == temp.Naam)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return this.Naam.GetHashCode();
        }



        public Person(string naam)
        {
            this.Naam = naam;
            this.Contact = new HashSet<Person>();
        }


        public static void SetupTracing()
        {
            List<Person> input = new List<Person>();

            Console.WriteLine("Welkom bij contact tracing!\nGeeft de namen op van de betrokken personen. Geef een witregel om te stoppen)");

            bool loop = true;
            string answer;

            while (loop)
            {
                answer = Console.ReadLine();
              

                if (answer == "")
                {
                    loop = false;
                }

                else
                {
                    input.Add(new Person(answer));
                }

                
            }

            foreach (Person persoon in input)
            {

                Console.WriteLine($"Geef de contacten van {persoon.Naam} gescheiden door komma's");
                string contacten = Console.ReadLine();
                string[] contactenarr = contacten.Split(",");
                foreach (string contact in contactenarr)
                {

                    foreach (Person element in input)
                    {
                        if (element.Naam == contact)
                        {
                            persoon.Contact.Add(element);
                        }
                    }

                }

            }

            Console.WriteLine("Voor wie wil je de contacten nagaan?");
            string toCheck = Console.ReadLine();
            Person p = null;
            foreach (Person element in input)
            {
                if (element.Naam == toCheck)
                {
                    p = element;
                }

            }


            Console.WriteLine("Hoe veel niveaus ver wil je zoeken? (1 = rechtstreekse contacten, 2 = contacten van contacten,...");
            int level = Convert.ToInt32(Console.ReadLine());
            HashSet < Person > contacts = new HashSet<Person>();
            p.Trace(contacts, level);
            foreach (var contact in contacts)
            {
                Console.WriteLine(contact.Naam);
            }


        }

        public void Trace(HashSet<Person> contacts, int level)
        {
            if (level == 0)
            {
                return;
            }
            if (level == 1)
            {
                foreach (var el in this.Contact)
                {
                    contacts.Add(el);
                }
            }

            else
            {
                foreach(var el in this.Contact)
                {
                    contacts.Add(el);
                    el.Trace(contacts, level - 1);
                }
            }

            











        }



    }

    
}
