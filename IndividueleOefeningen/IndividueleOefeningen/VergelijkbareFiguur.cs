﻿using System;
namespace IndividueleOefeningen
{
    public abstract class VergelijkbareFiguur
    {
        //attributen
        private ConsoleColor kleur;


        //properties

        public ConsoleColor Kleur
        {
            get
            {
                return this.kleur;
            }
            set
            {
                this.kleur = value;
            }
        }

        //abstract property (dus met get; en/of set;)

        public abstract double Oppervlakte { get; }

                  
    }
}
