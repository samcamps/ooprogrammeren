﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Immutable;


namespace IndividueleOefeningen
{
    class ExceptionHandling
    {

        public static void ToonSubmenu()
        {
            Console.WriteLine("Welke oefening runnen?\n1. Weekdagen\n2. Weekdagen met Error\n3. Overflow zonder exception\n4. Overflow met exception\n5. Keuze Element" +
                "\n6. Keuze Element extra voorzichtig\n7. Demonstreer Kat\n8. Demonstreer Lijst Katten" +
                "\n9. FileHelper\n10. Kat met Custom Exception");
            int keuze = Convert.ToInt32(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    DemonstreerFoutafhandelingWeekdagenZonderException();
                    break;

                case 2:

                    DemonstreerFoutafhandelingWeekdagenMetException();
                    break;

                case 3:

                    DemonstreerFoutafhandelingOverflowZonderException();
                    break;

                case 4:

                    DemonstreerFoutafhandelingOverflowMetException();
                    break;

                case 5:

                    DemonstreerKeuzeElement();
                    break;

                case 6:

                    DemonstreerKeuzeElementExtraVoorzichtig();
                    break;

                case 7:

                    DemonstreerLeeftijdKat();
                    break;

                case 8:

                    Kat.DemonstreerLeeftijdKatMetResourceCleanup();
                    break;
                case 9:

                    FileHelper();
                    break;

                case 10:

                    DemonstreerLeeftijdKatMetCE();
                    break;

                case 11:
                    try
                    {
                        DemonstreerFormulieren();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Fout in de contructor ergens");
                    }
                    break;

                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }



        private static void DemonstreerFoutafhandelingWeekdagenZonderException()
        {
            string[] arr = new string[5];
            arr[0] = "Vrijdag";
            arr[1] = "Maandag";
            arr[2] = "Dinsdag";
            arr[3] = "Woensdag";
            arr[4] = "Donderdag";

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(arr[i]);
            }
        }


        private static void DemonstreerFoutafhandelingWeekdagenMetException()
        {
            string[] arr = new string[5];
            arr[0] = "Vrijdag";
            arr[1] = "Maandag";
            arr[2] = "Dinsdag";
            arr[3] = "Woensdag";
            arr[4] = "Donderdag";

            try
            {
                for (int i = 0; i <= 5; i++)
                {
                    Console.WriteLine(arr[i].ToString());
                }
            }
            catch (Exception ex)
            {

            }

        }


        private static void DemonstreerFoutafhandelingOverflowZonderException()
        {
            int num1, num2;
            int resultaat;
            num1 = 30;
            num2 = 60;
            resultaat = Convert.ToInt32(num1 * num2);
            Console.WriteLine("{0} x {1} = {2}", num1, num2, resultaat);
        }


        private static void DemonstreerFoutafhandelingOverflowMetException()
        {

            int num1, num2;
            byte resultaat;
            num1 = 30;
            num2 = 60;

            try
            {
                resultaat = Convert.ToByte(num1 * num2);
                Console.WriteLine("{0} x {1} = {2}", num1, num2, resultaat);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Het getal is te groot om te converteren naar het gewenste formaat, biatch!");
            }


        }

        private static void DemonstreerKeuzeElement()
        {
            int[] arr = new int[3];
            Random rand = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }

            string answer = "ja";


            while (answer == "ja")
            {
                Console.WriteLine("Geef de index van het getal dat je wilt zien");


                try
                {
                    int keuze = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine($"Het getal is {arr[keuze]}\nWil je doorgaan?");
                    answer = Console.ReadLine().ToLower();
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Die index hebben we niet!");
                }

            }

            Console.WriteLine("ok ciao bye");
        }


        private static void DemonstreerKeuzeElementExtraVoorzichtig()
        {
            int[] arr = new int[3];
            Random rand = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }

            string answer = "ja";


            while (answer == "ja")
            {
                Console.WriteLine("Geef de index van het getal dat je wilt zien");

                try
                {
                    int keuze = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine($"Het getal is {arr[keuze]}\nWil je doorgaan?");
                    answer = Console.ReadLine().ToLower();
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Die index hebben we niet!");
                }

                catch (FormatException)
                {
                    Console.WriteLine("Formaat is niet correct");
                }

                catch (OverflowException)
                {
                    Console.WriteLine("Getal is muug te groot");
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }

            Console.WriteLine("ok ciao bye");
        }


        public static void DemonstreerLeeftijdKat()
        {
            try
            {
                //Kat kat = new Kat(27);
                Kat kat2 = new Kat(10);
                
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public static void FileHelper()
        {
            Console.WriteLine("Geef het pad naar de file");
            string pad = Console.ReadLine();

            try
            {
                string[] ingelezentekst = File.ReadAllLines(pad);
                foreach (var item in ingelezentekst)
                {
                    Console.WriteLine(item);
                }
            }
            catch (FileNotFoundException e)

            {
                Console.WriteLine("File kon niet gevonden worden");
            }

            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("File bestaat maar kon niet gelezen worden");
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Er is iets misgelopen, contacteer helpdesk");
            }

        }


        public static void DemonstreerLeeftijdKatMetCE()
        {
            try
            {
                KatMetCustomException kat = new KatMetCustomException(37);
            }
            catch (KatLeeftijdException ex)
            {
                Console.WriteLine($"{ex.MeegegevenWaarde} is geen geldige leeftijd. De laagst mogelijke leeftijd is {ex.LaagstMogelijkeWaarde} jaar, de hoogst mogelijke leeftijd is {ex.HoogstMogelijkeWaarde} jaar.");
            }

        }


        private static void DemonstreerFormulieren()
        {
            
           var vraag1 = new FormulierGetalVraag("Wat is je leeftijd", 18, 130);
           var vraag2 = new FormulierVrijeTekstVraag("Hoe ziet jouw ideale dag eruit?");
           var vraag3 = new FormulierGetalVraag("Hoe veel personen heb je ten laste?", 0, 10);
           var vraag4 = new FormulierVrijeTekstVraag("Wie is je idool?");

            List<FormulierVraag> test1 = new List<FormulierVraag>() { vraag1, vraag2 };


            Formulier f1 = new Formulier(test1);
            Formulier f2 = new Formulier(new List<FormulierVraag> { vraag3, vraag4 });

 

            try
            {
                f1.VulIn();
                f1.Toon();
            }
            catch (Exception)
            {
                System.Console.WriteLine("We zullen dit formulier weggooien.");
                f1 = null;
            }
            try
            {
                f2.VulIn();
                f2.Toon();
            }
            catch (Exception)
            {
                System.Console.WriteLine("We zullen dit formulier weggooien.");
                f2 = null;
            }
        }




    }
}