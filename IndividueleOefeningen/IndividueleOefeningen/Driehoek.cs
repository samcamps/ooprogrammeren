﻿using System;
namespace IndividueleOefeningen
{
    public class Driehoek
    {

        private double basis = 1.0;
        private double hoogte = 1.0;


        public Driehoek(double hoogte, double basis)
        {
            this.Hoogte = hoogte;
            this.Basis = basis;
        }

        public Driehoek()
        {

        }


        public double Basis
        {
            get
            {
                return this.basis;
            }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden om een basis van {value} in te stellen");
                }
                else
                {
                    this.basis = value;
                }
            }
        }

        public double Hoogte
        {
            get
            {
                return this.hoogte;
            }

            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden om een breedte van {value} in te stellen!");
                }
                else
                {
                    this.hoogte = value;
                }
            }
         
        }

        public string Oppervlakte
        {
            get
            {
                return $"Een driehoek met een basis van {this.basis}m en een hoogte van {this.hoogte}m heeft een oppervlakte van {(this.basis * this.hoogte)/2:F2}m²";
            }
        }


    }
}
