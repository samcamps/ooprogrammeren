﻿using System;
namespace IndividueleOefeningen
{
    abstract class FormulierVraag
    {

        private string tekst;
        private string antwoord;


        public string Tekst
        {
            get
            {
                return tekst;
            }
            set
            {   
                tekst = value;
            }
        }


        public string Antwoord
        {

            get
            {
                return antwoord;
            }
            set
            {
                if (value is null)
                {
                    throw new ArgumentException("Vraag mag je niet null maken");   
                }
                else
                {
                    antwoord = value;
                }
            }
        }


        abstract public void Toonvraag();
        abstract public void LeesAntwoord();



        public FormulierVraag(string tekst)
        {
            if (tekst is null)
            {
                throw new ArgumentException("Vraag mag niet leeg zijn");
            }
            this.Tekst = tekst;

        }



    }
}
