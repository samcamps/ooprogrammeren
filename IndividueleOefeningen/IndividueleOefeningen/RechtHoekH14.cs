﻿using System;
namespace IndividueleOefeningen
{
    public class RechtHoekH14:ParallellogramH14
    {

        public double Breedte
        {
            get
            {
                return base.Hoogte;
            }

            set
            {
                base.Hoogte = value;
            }
        }

        public double Lengte
        {
            get
            {
                return base.Basis;
            }

            set
            {
                base.Basis = value;
            }
        }

      

        public RechtHoekH14() : base (0,0)
        {
        }

        public RechtHoekH14(double breedte, double lengte) : base(lengte,breedte)
        {
            this.Breedte = breedte;
            this.Lengte = lengte;
        }

    }
}
