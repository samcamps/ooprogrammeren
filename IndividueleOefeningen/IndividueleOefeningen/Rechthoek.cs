﻿using System;
namespace IndividueleOefeningen
{
    public class Rechthoek
    {
        private double breedte = 1.0;
        private double hoogte = 1.0;



        public Rechthoek(double hoogte, double breedte)
        {
            this.Hoogte = hoogte;
            this.Breedte = breedte;
        }

        public Rechthoek()
        {

        }




        public double Breedte
        {

            get
            {
                return this.breedte;
            }


            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden om een breedte van {value} in te stellen!");
                }
                else
                {
                    this.breedte = value;
                }

            }
        }


        public double Hoogte
        {
            get
            {
                return this.hoogte;
            }

            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden om een breedte van {value} in te stellen!");
                }
                else
                {
                    this.hoogte = value;
                }
            }
        }


        public string Oppervlakte
        {
            get
            {
                return $"Een rechthoek met een breedte van {this.breedte}m en een hoogte van {this.hoogte}m heeft een oppervlakte van {this.hoogte * this.breedte:F1}m²";
                
            }
        }


    }
}
