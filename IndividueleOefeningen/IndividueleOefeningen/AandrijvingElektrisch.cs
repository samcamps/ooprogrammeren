﻿using System;
namespace IndividueleOefeningen
{
    public class AandrijvingElektrisch: IAandrijving
    {


        public void EnergieToevoegen()
        {
            Console.WriteLine($"EnergieToevoegen - Elektrisch");

        }


        public void Vertragen(int kmPerUurPerSeconde, int doelsnelheid)
        {
            Console.WriteLine($"Vertragen - Elektrisch");
        }


        public void Versnellen(int kmPerUurPerSeconde, int doelsnelheid)
        {
            Console.WriteLine($"Versnellen Elektrisch");
        }



        public AandrijvingElektrisch()
        {
        }
    }
}
